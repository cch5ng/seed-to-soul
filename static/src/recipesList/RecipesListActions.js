import { receivePostLogin } from '../registration/RegistrationActions'

const API_GET_RECIPE_LIST_CUR = 'http://localhost:5000/api/recipes'
const API_POST_RECIPE_LIST_CUR = 'http://localhost:5000/api/recipes/update'
export const REQUEST_RECIPE_LIST_CUR = 'REQUEST_RECIPE_LIST_CUR';
export const RECEIVE_RECIPE_LIST_CUR = 'RECEIVE_RECIPE_LIST_CUR';
export const REQUEST_RECIPE_UPDATE = 'REQUEST_RECIPE_UPDATE';
export const RECEIVE_RECIPE_UPDATE = 'RECEIVE_RECIPE_UPDATE';


export function requestRecipeListCur() {
  return {
    type: REQUEST_RECIPE_LIST_CUR,
    retrieving: true
  }
}

export function receiveRecipeListCur(recipeListCur) {
  return {
    type: RECEIVE_RECIPE_LIST_CUR,
    recipeListCur,
    retrieving: false
  }
}

/*
  @param {int} user_id
*/
export const getRecipeList = () => dispatch => {
  let accessToken = localStorage.getItem('access_token');
  console.log('accessToken: ' + accessToken);
  dispatch(requestRecipeListCur());
  return fetch(`${API_GET_RECIPE_LIST_CUR}`, {
    method: 'POST',
    headers: {
      "Authorization": `JWT ${accessToken}`
    },
  })
    .then(response => response.json())
    .then(json => {
      dispatch(receiveRecipeListCur(json));
      dispatch(receivePostLogin({"token": accessToken}));
    })
    .catch(function(err) {
      console.log('fetch err: ' + err.message)
    });
}


export function requestRecipeUpdate() {
  return {
    type: REQUEST_RECIPE_UPDATE,
    retrieving: true
  }
}

export function receiveRecipeUpdate(updatedRecipe) {
  return {
    type: RECEIVE_RECIPE_UPDATE,
    updatedRecipe,
    retrieving: false
  }
}

/*
  @param {obj} queryObj {'recipe_id': str, 'saved': bool, 'deleted': bool, 'user_rating': int}
*/
export const postRecipeUpdate = (queryObj) => dispatch => {
  dispatch(requestRecipeUpdate());
  return fetch(`${API_POST_RECIPE_LIST_CUR}`, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json',
    },
    body: JSON.stringify(queryObj)
  })
    .then(response => response.json())
    .then(json => dispatch(receiveRecipeUpdate(json)))
    .catch(function(err) {
      console.log('fetch err: ' + err.message)
    });
}
