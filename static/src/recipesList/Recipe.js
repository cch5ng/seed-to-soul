import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Modal, Button } from 'react-bootstrap';
import { getRecipeList, postRecipeUpdate } from '../recipesList/RecipesListActions';

class Recipe extends Component {

  state = {
    show: false
  }

  formatTime = this.formatTime.bind(this)
  handleHide = this.handleHide.bind(this);
  // handleClose = this.handleClose.bind(this)
  handleShow = this.handleShow.bind(this)


  handleHide() {
    this.setState({ show: false });
  }

  // handleClose() {
  //   this.setState({ show: false });
  // }

  handleShow() {
    this.setState({ show: true });
  }

  // HELPERS
  /*
   * @param seconds (integer)
   * @return time (string)
   * h hours, m minutes or m minutes
   */
  formatTime(sec) {
    let minutes
    let hours = 0
    let timeStr = ''

    if (sec > 3600) {
      hours = Math.floor(sec / 3600)
      minutes = (sec % 3600) / 60
    } else {
      minutes = Math.ceil(sec / 60)
    }

    if (hours >= 2) {
      timeStr = `${hours} hr ${minutes} min`
    } else if (hours >= 1) {
      timeStr = `${hours} hr ${minutes} min`
    } else {
      timeStr = `${minutes} min`
    }

    return timeStr
  }

  // mobile: 2 col grid or 1 col grid with img; either overlay or below have a 
  //   short description like: name, carb, total cook time, ???
  // tablet/desktop: 4 col or more (img)
  // interaction should be (save/hide/rate); maybe hover? to get an animation 
  //   slide in with the ingredients? (slide down maybe?)


  render() {
    let recipe = this.props.recipe
    console.log('recipe' + recipe)
    let linkUrl = `http://www.yummly.co/recipe/${recipe.yummly_recipe_id}`
    let altStr = `${recipe.name} photo`
    let imgSmUrl = `${recipe.img_url_sm}`
    let imgMdUrl = `${recipe.img_url_lg}` || `${recipe.img_url_md}` || `${recipe.img_url_sm}`
    let saveBtnId = `btnSave&&${recipe.yummly_recipe_id}`
    let deleteBtnId = `btnHide&&${recipe.yummly_recipe_id}`
    let ratingId = `rating&&${recipe.yummly_recipe_id}`
    let ingredientsStr = '';
    let ingredientsCnt = 0;
    if (recipe.ingredients) {
      ingredientsStr = recipe.ingredients.join(', ');
      ingredientsCnt = recipe.ingredients.length;
    }
    let saveClass = recipe.saved ? "btn-all btn-saved" : "btn-all btn-not-saved"
    let hideClass = recipe.hidden ? "btn-hidden" : "btn-not-hidden"
    let btnName = recipe.saved ? "Clear" : "Save"

    return (
      <div className="recipe-wrapper-in">
        <div className="recipe-div1">
          <a href={linkUrl} target="_blank">
            <picture>
              <source srcset={imgMdUrl} media="(min-width: 768px)" />
              <img src={imgMdUrl} alt="recipe photo"  className="recipe-img" />
            </picture>
          </a>
        </div>
        <div className="recipe-div2">
          <div class="recipe-heading">
            <div class="recipe-title">
              <h2 className="recipe-name"><a href={linkUrl} target="_blank">{recipe.name}</a></h2>
              <button className={saveClass}
                id={saveBtnId} onClick={(ev) => this.props.recipeBtnClickHandler(ev.target.id, recipe.saved)}>{btnName}</button>
              <label>
                Rate
                <input type="number" id={ratingId} className="input-all input-rating"
                 value={recipe.user_rating} min="1" max="5" 
                 onChange={(ev) => this.props.recipeUserRatingChangeHandler(ev.target.id, ev.target.value)} />
              </label>
            </div>
            <div class="recipe-cooktime">
              <span class="vertical-center subheading">{this.formatTime(recipe.total_time)}</span>
            </div>
          </div>
        </div>
        <div className="recipe-div3 recipe-nutrients">
          {recipe.sugar && (<p className="nutrient-item"><span className="subheading">Sugar</span> {recipe.sugar}g</p>) }
          {recipe.carbs && (<p className="nutrient-item"><span className="subheading">Carbs</span> {recipe.carbs}g</p>) }
          {recipe.calories && (<p className="nutrient-item"><span className="subheading">Cal</span> {recipe.calories}kcal</p>)}
        </div>

        {ingredientsCnt > 0 && (
          <div className="modal-container" style={{ height: 50 }}>
            <Button onClick={this.handleShow}>
              {ingredientsCnt} Ingredients
            </Button>

            <Modal show={this.state.show} onHide={this.handleHide} container={this}
              aria-labelledby="contained-modal-title">
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title" >Ingredients</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <ul>
                {recipe.ingredients.map(i => (
                  <li className="li-ingredient">{i}</li>
                ))}
                </ul>
              </Modal.Body>
            </Modal>
          </div>
        )}
      </div>
    );
  }
}

Recipe.propTypes = {
  recipeBtnClickHandler: PropTypes.func.isRequired,
  recipeUserRatingChangeHandler: PropTypes.func.isRequired,
};

export default Recipe
