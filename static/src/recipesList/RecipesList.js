import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getRecipeList, postRecipeUpdate } from '../recipesList/RecipesListActions';
import Recipe from '../recipesList/Recipe';

class RecipesList extends Component {

  state = {
    recipesFilter: 'allRecipes',
    sortRecipes: 'date-recent-old',
    triggerRerender: false
  }

  handleInputChange = this.handleInputChange.bind(this)

  componentDidMount() {
    console.log('RecipesList componentDidMount')
    this.props.dispatch(getRecipeList());
  }

  /*
    @param { onChange event }
    @return { none }; updates state
    Event handler for all inputs (except save button). TODO ? REFACTOR?
  */
  handleInputChange(ev) {
    const target = ev.target;
    let value;

    if (target.type === 'checkbox') {
      value = target.checked
    }
    if (target.type === 'radio' || target.type === 'select-one') {
      value = target.value
    }
    const name=target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    let recipesListAr = []

    if (this.props.registration) {
      console.log('this.props.registration: ' + this.props.registration)
    }

    // REFACTOR less nested data
    // TODO lookup typescript use of !
    // && this.props.recipesList!.recipesList!.recipesList)
    if (this.props.recipesList && this.props.recipesList.recipesList && this.props.recipesList.recipesList.recipesList) {
      let recipesListObj = this.props.recipesList.recipesList.recipesList
      Object.keys(recipesListObj).forEach(k => {
        recipesListAr.push(recipesListObj[k])
      })
      console.log('recipesListAr: ' + recipesListAr)

      if (this.state.recipesFilter === 'saved') {
        recipesListAr = recipesListAr.filter(r => {return r.saved === true})
      }

      // TODO refactor b/c the sorting logic will look very redundant so figure a way to abstract the logic
      // sorting logic from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort
      if (this.state.sortRecipes === 'rating-low-high') {
        recipesListAr.sort(function (a, b) {
          return a.user_rating - b.user_rating;
        });
      }
      if (this.state.sortRecipes === 'rating-high-low') {
        recipesListAr.sort(function (a, b) {
          return b.user_rating - a.user_rating;
        });
      }
      // TODO refactor
      if (this.state.sortRecipes === 'date-old-recent') {
        recipesListAr.sort(function (a, b) {
          return Date.parse(a.delivery_date) - Date.parse(b.delivery_date);
        });
      }
      if (this.state.sortRecipes === 'date-recent-old') {
        recipesListAr.sort(function (a, b) {
          return Date.parse(b.delivery_date) - Date.parse(a.delivery_date);
        });
      }
      // TODO refactor next 2 sections, pretty much the same logic except reverse
      if (this.state.sortRecipes === 'alpha-a-z') {
        recipesListAr.sort(function (a, b) {
          let nameA = a.name.toUpperCase(); // ignore upper and lowercase
          let nameB = b.name.toUpperCase(); // ignore upper and lowercase
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }

          // names must be equal
          return 0;
        });
      }
      if (this.state.sortRecipes === 'alpha-z-a') {
        recipesListAr.sort(function (b, a) {
          let nameA = a.name.toUpperCase();
          let nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }

          // names must be equal
          return 0;
        });
      }
      // TODO refactor
      if (this.state.sortRecipes === 'least-most') {
        // bug where some seeded data missing ingreds
        let recipesNoIngreds = recipesListAr.filter(r => (!r.ingredients))

        let recipesIngreds = recipesListAr.filter(r => (r.ingredients))
        recipesIngreds.sort(function (a, b) {
          return a.ingredients.length - b.ingredients.length;
        });
        recipesListAr = recipesIngreds.concat(recipesNoIngreds);
      }
    }

    return (
      <div className="">
        <h1>Recipes</h1>
        <form className="flex-row">
          <div className="flow-column-1">
            <h2>Filter</h2>
            <input type="radio" name="recipesFilter" value="allRecipes"
              onChange={this.handleInputChange}
              checked={"allRecipes" === this.state.recipesFilter} /> All recipes
            <input type="radio" name="recipesFilter" value="saved"
              onChange={this.handleInputChange}
              checked={"saved" === this.state.recipesFilter} /> Saved recipes
          </div>
          <div className="flow-column-2">
            <h2>Sort</h2>
            <select name="sortRecipes" value={this.state.sortRecipes} onChange={this.handleInputChange}>
              <option value="date-recent-old">Most recent to oldest</option>
              <option value="date-old-recent">Oldest to most recent</option>
              <option value="rating-high-low">Rating (highest to lowest)</option>
              <option value="rating-low-high">Rating (lowest to highest)</option>
              <option value="alpha-a-z">Alphabetically (A - Z)</option>
              <option value="alpha-z-a">Alphabetically (Z - A)</option>
              <option value="least-most">Ingredients Count (least to most)</option>
            </select>
          </div>
        </form>
        <ul className="recipe-container">
        {recipesListAr.map(recipe => (
          <Recipe recipe={recipe} key={recipe.yummly_recipe_id}
            recipeBtnClickHandler={this.props.recipeBtnClickHandler}
            recipeUserRatingChangeHandler={this.props.recipeUserRatingChangeHandler}
          />
        ))}
        </ul>
      </div>
    )}
}

function mapStateToProps(recipesList) {
  return { recipesList }
}

RecipesList.propTypes = {
  recipeBtnClickHandler: PropTypes.func.isRequired,
  recipeUserRatingChangeHandler: PropTypes.func.isRequired,
};

export default connect(mapStateToProps)(RecipesList);
