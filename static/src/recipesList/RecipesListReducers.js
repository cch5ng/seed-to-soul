import { REQUEST_RECIPE_LIST_CUR, RECEIVE_RECIPE_LIST_CUR,
  REQUEST_RECIPE_UPDATE, RECEIVE_RECIPE_UPDATE
 } from '../recipesList/RecipesListActions';

export function recipesList(state = {}, action) {
  switch(action.type) {
    case REQUEST_RECIPE_LIST_CUR:
      return {
        ...state,
        retrieving: action.retrieving,
      }
    case RECEIVE_RECIPE_LIST_CUR:
      let recipesListObj = {}
      action.recipeListCur.forEach(recipe => {
        recipesListObj[recipe.yummly_recipe_id] = recipe
      })
      return {
        ...state,
        recipesList: recipesListObj,
        retrieving: action.retrieving,
      }

    case REQUEST_RECIPE_UPDATE:
      return {
        ...state,
        retrieving: action.retrieving,
      }
    case RECEIVE_RECIPE_UPDATE:
      if (action.updatedRecipe['saved'] === true || action.updatedRecipe['saved'] === false) {
      }
      // let id = action.recipe_id
      if (action.updatedRecipe['saved'] === true || action.updatedRecipe['saved'] === false && action.updatedRecipe.saved !== null) {
        return {
          ...state,
          recipesList: {...state.recipesList,
            [action.updatedRecipe.recipe_id]: {...state.recipesList[action.updatedRecipe.recipe_id],
              saved: action.updatedRecipe.saved}
          },
          retrieving: action.retrieving,
        }
      }
      if (action.updatedRecipe['hidden'] === true || action.updatedRecipe['hidden'] === false && action.updatedRecipe.hidden !== null) {
        return {
          ...state,
          recipesList: {...state.recipesList,
            [action.updatedRecipe.recipe_id]: {...state.recipesList[action.updatedRecipe.recipe_id],
              hidden: action.updatedRecipe.hidden}
          },
          retrieving: action.retrieving,
        }
      }
      if (action.updatedRecipe['user_rating']) {
        return {
          ...state,
          recipesList: {...state.recipesList,
            [action.updatedRecipe.recipe_id]: {...state.recipesList[action.updatedRecipe.recipe_id],
              user_rating: action.updatedRecipe.user_rating}
          },
          retrieving: action.retrieving,
        }
      }
    default:
      return state
  }
}
