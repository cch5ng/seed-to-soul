
const API_GET_RECIPE_LIST = 'http://localhost:5000/api/recipe_quick_search'
export const REQUEST_QUICK_RECIPES = 'REQUEST_QUICK_RECIPES';
export const RECEIVE_QUICK_RECIPES = 'RECEIVE_QUICK_RECIPES';

export function requestQuickRecipes() {
  return {
    type: REQUEST_QUICK_RECIPES,
    retrieving: true
  }
}

export function receiveQuickRecipes(result) {
  let recipes = result.matches
  return {
    type: RECEIVE_QUICK_RECIPES,
    recipes,
    retrieving: false
  }
}

// build query string on front end
export const fetchQuickRecipes = (queryObj) => dispatch => {
  dispatch(requestQuickRecipes());
  return fetch(API_GET_RECIPE_LIST, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json',
    },
    body: JSON.stringify(queryObj)
  })
    .then(response => response.json())
    .then(json => dispatch(receiveQuickRecipes(json)))
    .catch(function(err) {
      console.log('fetch err: ' + err.message)
    });
}
