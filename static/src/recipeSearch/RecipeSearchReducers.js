
import { REQUEST_QUICK_RECIPES, RECEIVE_QUICK_RECIPES } from '../recipeSearch/RecipeSearchActions';

const initState = { recipes: {} }

export function recipeSearch(state = initState, action) {
  switch(action.type) {
    case REQUEST_QUICK_RECIPES:
      return {
        ...state,
        retrievingQuickRecipes: action.retrieving,
      }
    case RECEIVE_QUICK_RECIPES:
      let recipesObj = {}
      action.recipes.forEach(recipe => {
        recipesObj[recipe.id] = recipe
      })
      return {
        ...state,
        recipes: recipesObj,
        retrievingQuickRecipes: action.retrieving,
      }
    default:
      return state
  }
}
