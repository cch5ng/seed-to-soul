import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Select from 'react-select';
import VirtualizedSelect from 'react-virtualized-select';
import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import { fetchQuickRecipes, updateRecipeSettings, getRecipeSettings } from '../recipeSearch/RecipeSearchActions'

// QUEUE 022218 should I be getting the field metadata from the BE and populating
// each input dynamically? how many backend requests would I want to make? (one
// per metadata set or one for all? would I want to store that resulting data into
// component state or redux)

// QUEUE 022218 try to ask Henry about redux timing
// if my view is dependent on an async request, when I receive it, if I update
// component state, this triggers re-render()
// how does timing of view refresh work when using redux (I feel like even when
// store gets updated in async manner, component is connected to store, sometimes
// the view doesn't get refreshed; do I need to do componentWillReceiveProps?)

class RecipeSearch extends Component {
// TODO make form as controlled component
//   define state (values) for each input
//   define input onChange handlers (bind to this)
//   add onChange attr to input fields

  state = {
    maxCarbs: 0,
    //maxSugar: 0,
    minVitA: 0,
    minVitC: 0,
    minVitE: 0,
    minVitK: 0,
    american: false,
    italian: false,
    mexican: false,
    southwestern: false,
    asian: false,
    indian: false,
    chinese: false,
    thai: false,
    japanese: false,
    hawaiian: false,
    mediterranean: false,
    greek: false,
    cuban: false,
    Gluten: false,
    Peanut: false,
    Seafood: false,
    Sesame: false,
    Soy: false,
    Dairy: false,
    Egg: false,
    Sulfite: false,
    TreeNut: false,
    Wheat: false,
    recipeName: '',
    lactoVegetarian: false,
    ovoVegetarian: false,
    pescetarian: false,
    vegan: false,
    lactoOvoVeg: false,
    paleo: false,
    selectedOption: '',
    excludedIngredients: [],
    selectedOptionIncIngred: '',
    '393^Gluten-Free': false,
  }

  handleInputChange = this.handleInputChange.bind(this);
  handleSaveBtnClick = this.handleSaveBtnClick.bind(this);
  diffCuisinesSettingsToState = this.diffCuisinesSettingsToState.bind(this);
  handleChange = this.handleChange.bind(this);

  /*
    @param { onChange event }
    @return { none }; updates state
    Event handler for all inputs (except save button). Updates component state
    following React controlled form component pattern. Dependent on setting input
    name attribute and referencing this function as the onChange attribute handler.
    Ref pattern: https://reactjs.org/docs/forms.html#handling-multiple-inputs
    Previously I was writing a unique input change handler per form control but
    the documented pattern feels cleaner.
    Note: the nice thing about this method is that it can be abstracted to apply to most 
    of the form inputs. The negative thing is that if I'd like to categorize groups of
    state properties (nutrients, allergies, excluded ingredients, then I would
    probably need to write a separate onChange handler for each category group).
  */
  handleInputChange(ev) {
    const target = ev.target;
    let value;

    if (target.type === 'checkbox') {
      value = target.checked
    }
    if (target.type === 'number' || target.type === 'text' || target.type === 'radio') {
      value = target.value
    }
    const name=target.name;

    this.setState({
      [name]: value
    });
  }

  /*
   * @param
   * @result
   * Event handler for Excluded Ingredients input control. When user selects
   * an item from auto-suggest list, it will be added to comp state (array of
   * objects).
   */
  handleChange(selectedOption) {
    this.setState({ excludedIngredients: selectedOption,});
    console.log('this.state.excludedIngredients: ' + this.state.excludedIngredients);
  }

  /*
    @param { onClick event }
    @return { none }; triggers Redux store update/action which also triggers
    internal API call to update DB
    Event handler for Save button. Uses component state
    following React controlled form component pattern. Dependent on setting input
    onClick attribute and referencing this function as the handler.
  */
  handleSaveBtnClick(ev) {
    ev.preventDefault();
    const ALL_YUMMLY_CUISINES = ["asian",
                                 "american",
                                 "italian",
                                 "mexican",
                                 "southwestern",
                                 "indian",
                                 "chinese",
                                 "thai",
                                 "japanese",
                                 "hawaiian",
                                 "mediterranean",
                                 "greek",
                                 "cuban",];
    const JS_ALLERGY_TO_YUMMLY = {"Gluten": "Gluten-Free",
                                  "Peanut": "Peanut-Free",
                                  "Seafood": "Seafood-Free",
                                  "Sesame": "Sesame-Free",
                                  "Soy": "Soy-Free",
                                  "Dairy": "Dairy-Free",
                                  "Egg": "Egg-Free",
                                  "Sulfite": "Sulfite-Free",
                                  "TreeNut": "Tree Nut-Free", //exception
                                  "Wheat": "Wheat-Free"}
    const JS_NUTRIENT_TO_YUMMLY_ATTR = {"vitA": "VITA_RAE",
                                        "vitC": "VITC",
                                        "vitE": "TOCPHA",
                                        "vitK": "VITK"}
    const JS_DIET_TO_YUMMLY_ID = {"lactoVegetarian": "388",
                                  "ovoVegetarian": "389",
                                  "pescetarian": "390",
                                  "vegan": "386",
                                  "lactoOvoVeg": "387",
                                  "paleo": "403",}

    let queryObj = {};
    let cuisineList = [];
    let allergiesList = [];
    let excludedIngredientsList;
    let maxCarbs = this.state.maxCarbs;
    let dietsList = [];
    //let notifyDay = this.state.notifyDay;

    console.log('cuisineList: ' + cuisineList)
    ALL_YUMMLY_CUISINES.forEach(c => {
      if (this.state[c] === true) {
        cuisineList.push(c)
      }
    })

    Object.keys(JS_ALLERGY_TO_YUMMLY).forEach(a => {
      if (this.state[a]) {
        allergiesList.push(JS_ALLERGY_TO_YUMMLY[a])
      }
    })

    Object.keys(JS_DIET_TO_YUMMLY_ID).forEach(d => {
      if (this.state[d]) {
        dietsList.push(JS_DIET_TO_YUMMLY_ID[d])
      }
    })

    excludedIngredientsList = this.state.excludedIngredients.map((ing) => {
      return ing.value;
    });

    queryObj['maxCarbs'] = maxCarbs;
    let yummly_attr_vita = JS_NUTRIENT_TO_YUMMLY_ATTR["vitA"];
    queryObj[yummly_attr_vita] = this.state.minVitA;
    let yummly_attr_vitc = JS_NUTRIENT_TO_YUMMLY_ATTR["vitC"];
    queryObj[yummly_attr_vitc] = this.state.minVitC;
    let yummly_attr_vite = JS_NUTRIENT_TO_YUMMLY_ATTR["vitE"];
    queryObj[yummly_attr_vite] = this.state.minVitE;
    let yummly_attr_vitk = JS_NUTRIENT_TO_YUMMLY_ATTR["vitK"];
    queryObj[yummly_attr_vitk] = this.state.minVitK;
    // only add item to payload if data is available
    if (cuisineList.length) {
      queryObj['cuisine'] = cuisineList;
    }
    if (allergiesList.length) {
      queryObj['allergiesList'] = allergiesList;
    }
    if (dietsList.length) {
      queryObj['dietsList'] = dietsList;
    }
    if (excludedIngredientsList.length) {
      queryObj['excludedIngredientsList'] = excludedIngredientsList;
    }
    if (this.state.recipeName) {
      queryObj['recipeName'] = this.state.recipeName
    }

    console.log('queryObj: ' + queryObj)
    console.log('keys queryObj: ' + Object.keys(queryObj))

    this.props.dispatch(fetchQuickRecipes(queryObj));
  }

  // HELPERS

  /*
   * @param {propsCuisinesAr} - array of cuisines saved in db for recipe settings
   * (of cur user)
   * Given cuisines array, performs a diff between all possible cuisines vs
   * user's saved cuisines. Returns an object with object format needed to update
   * component state for the field values.
   */
  diffCuisinesSettingsToState(propsCuisinesAr) {
    const ALL_YUMMLY_CUISINES = ["asian",
                                 "american",
                                 "italian",
                                 "mexican",
                                 "southwestern",
                                 "indian",
                                 "chinese",
                                 "thai",
                                 "japanese",
                                 "hawaiian",
                                 "mediterranean",
                                 "greek",
                                 "cuban",];

    let newCuisinesState = {};

    ALL_YUMMLY_CUISINES.forEach(c => {
      if (propsCuisinesAr.indexOf(c) > -1) {
        newCuisinesState[c] = true;
      } else {
        newCuisinesState[c] = false;
      }
    })

    return newCuisinesState;
  }

  /* 
   * @param
   * @return
   * Used by react-select (auto-suggest component) to fetch the list of
   * supported ingredients to exclude (for yummly search api query).
   *
   */
  getOptions() {
    return fetch('/api/recipe_search/ingredients.json')
      .then((resp) => {
        return resp.json();
      }).then((json) => {
        return { options: json };
      })
  }

  /*
   * @param seconds (integer)
   * @return time (string)
   * h hours, m minutes or m minutes
   * TO REFACTOR
   */
  formatTime(sec) {
    let minutes
    let hours = 0
    let timeStr = ''

    if (sec > 3600) {
      hours = Math.floor(sec / 3600)
      minutes = (sec % 3600) / 60
    } else {
      minutes = Math.ceil(sec / 60)
    }

    if (hours >= 2) {
      timeStr = `${hours} hours and ${minutes} minutes`
    } else if (hours >= 1) {
      timeStr = `${hours} hour and ${minutes} minutes`
    } else {
      timeStr = `${minutes} minutes`
    }

    return timeStr
  }

  /* TO ADD

            <label>
            Max Sugar (grams)
            <input type="number" name="maxSugar" className="" 
             value={this.state.maxSugar} min="0"
             onChange={this.handleInputChange} />
          </label><br /><br />

  */

  /*

                  <li>
                  <a href={linkUrl} target="_blank">{r.recipeName}</a><br />
                  <a href={linkUrl} target="_blank">Total Cook Time: {cookTimeMin} (minutes)</a><br />
                  <a href={linkUrl} target="_blank">yummly link</a><br />
                  Ingredients: {ingredientsStr}<br />
                  {imgUrl && (
                      <a href={linkUrl} target="_blank"><img src={imgUrl} alt={altStr} /></a>
                  )}<br />
                </li>

  */

  render() {
    const { selectedOption, excludedIngredients } = this.state;
    let recipesList = [];
    if (this.props.recipeSearch && this.props.recipeSearch.recipeSearch && this.props.recipeSearch.recipeSearch.recipes) {
      let recipes = this.props.recipeSearch.recipeSearch.recipes
      recipesList = Object.keys(recipes).map(rid => (
        recipes[rid]
      ))
      console.log('recipes: ' + recipes)
    }

    // used by react virtualizer select
    const value = selectedOption && selectedOption.value;

    return (
      <div className="">
        <h1>Recipe Search</h1>
        <form className="">
          <h2 className="h2-preference">General</h2>
          <label>
            Recipe Name
            <input type="text" name="recipeName" className="input-text-recipe" 
             value={this.state.recipeName} placeholder="recipe name or ingredient"
             onChange={this.handleInputChange} />
          </label>

          <h2 className="h2-preference">Nutrients</h2>
          <label>
            Max Carbs (g)
            <input type="number" name="maxCarbs" className="" 
             value={this.state.maxCarbs} min="0"
             onChange={this.handleInputChange} />
          </label><br /><br />

          <label>
            Min Vitamin A (g)
            <input type="number" name="minVitA" className="" 
             value={this.state.minVitA} min="0"
             onChange={this.handleInputChange} />
          </label><br /><br />

          <label>
            Min Vitamin C (g)
            <input type="number" name="minVitC" className="" 
             value={this.state.minVitC} min="0"
             onChange={this.handleInputChange} />
          </label><br /><br />

          <label>
            Min Vitamin E (g)
            <input type="number" name="minVitE" className="" 
             value={this.state.minVitE} min="0"
             onChange={this.handleInputChange} />
          </label><br /><br />

          <label>
            Min Vitamin K (g)
            <input type="number" name="minVitK" className="" 
             value={this.state.minVitK} min="0"
             onChange={this.handleInputChange} />
          </label><br /><br />

          <h2 className="h2-preference">Ethnic Cuisine Preferences</h2>
          <label>
            <input type="checkbox" name="asian" value={this.state.asian}
             checked={this.state.asian} onChange={this.handleInputChange} />Asian
          </label>
          <label>
            <input type="checkbox" name="chinese" value={this.state.chinese}
             checked={this.state.chinese} onChange={this.handleInputChange} />Chinese
          </label>
          <label>
            <input type="checkbox" name="japanese" value={this.state.japanese}
             checked={this.state.japanese} onChange={this.handleInputChange} />Japanese
          </label>
          <label>
            <input type="checkbox" name="hawaiian" value={this.state.hawaiian}
             checked={this.state.hawaiian} onChange={this.handleInputChange} />Hawaiian
          </label><br /><br />

          <label>
            <input type="checkbox" name="mexican"  value={this.state.mexican}
             checked={this.state.mexican} onChange={this.handleInputChange} />Mexican
          </label>
          <label>
            <input type="checkbox" name="italian"  value={this.state.italian}
             checked={this.state.italian} onChange={this.handleInputChange} />Italian
          </label>
          <label>
            <input type="checkbox" name="mediterranean"  value={this.state.mediterranean}
               checked={this.state.mediterranean} onChange={this.handleInputChange} />Mediterranean
          </label>

          <details className="details-cuisines">
            <summary><span class="subheading">More Cuisines</span>
            </summary>
            <label>
              <input type="checkbox" name="thai" value={this.state.thai}
               checked={this.state.thai} onChange={this.handleInputChange} />Thai
            </label>
            <label>
              <input type="checkbox" name="indian" value={this.state.indian}
               checked={this.state.indian} onChange={this.handleInputChange} />Indian
            </label><br /><br />
            <label>
              <input type="checkbox" name="southwestern"  value={this.state.southwestern}
               checked={this.state.southwestern} onChange={this.handleInputChange} />Southwestern
            </label>
            <label>
              <input type="checkbox" name="cuban"  value={this.state.cuban}
               checked={this.state.cuban} onChange={this.handleInputChange} />Cuban
            </label><br /><br />
            <label>
              <input type="checkbox" name="american"  value={this.state.american}
               checked={this.state.american} onChange={this.handleInputChange} />American
            </label>
            <label>
              <input type="checkbox" name="greek"  value={this.state.greek}
               checked={this.state.greek} onChange={this.handleInputChange} />Greek
            </label>
          </details>

          <h2 className="h2-preference">Allergies</h2>
          <label>
            <input type="checkbox" name="Gluten" value={this.state.Gluten}
             checked={this.state.Gluten} onChange={this.handleInputChange} />Gluten
          </label>
          <label>
            <input type="checkbox" name="Peanut"  value={this.state.Peanut}
             checked={this.state.Peanut} onChange={this.handleInputChange} />Peanut
          </label>
          <label>
            <input type="checkbox" name="Seafood"  value={this.state.Seafood}
             checked={this.state.Seafood} onChange={this.handleInputChange} />Seafood
          </label>
          <label>
            <input type="checkbox" name="Soy"  value={this.state.Soy}
             checked={this.state.Soy} onChange={this.handleInputChange} />Soy
          </label>
          <label>
            <input type="checkbox" name="Dairy"  value={this.state.Dairy}
             checked={this.state.Dairy} onChange={this.handleInputChange} />Dairy
          </label>

          <details className="details-allergies">
            <summary><span class="subheading">More Allergies</span>
            </summary>
            <label>
              <input type="checkbox" name="Wheat"  value={this.state.Wheat}
               checked={this.state.Wheat} onChange={this.handleInputChange} />Wheat
            </label>
            <label>
              <input type="checkbox" name="Egg"  value={this.state.Egg}
               checked={this.state.Egg} onChange={this.handleInputChange} />Egg
            </label>
            <label>
              <input type="checkbox" name="Sesame"  value={this.state.Sesame}
               checked={this.state.Sesame} onChange={this.handleInputChange} />Sesame
            </label>
            <label>
              <input type="checkbox" name="Sulfite"  value={this.state.Sulfite}
               checked={this.state.Sulfite} onChange={this.handleInputChange} />Sulfite
            </label>
            <label>
              <input type="checkbox" name="TreeNut"  value={this.state.TreeNut}
               checked={this.state.TreeNut} onChange={this.handleInputChange} />Tree Nut
            </label>
          </details>

          <h2 className="h2-preference">Ingredients</h2>
          <label>Excluded Ingredients</label>
          <VirtualizedSelect
            async
            loadOptions={this.getOptions}
            multi
            labelKey='label'
            name="excluded_ingredients"
            value={excludedIngredients}
            minimumInput={1}
            onChange={this.handleChange}
            delimiter=','
            joinValues='true'
          />

          <h2 className="h2-preference">Diets</h2>
          <label>
            <input type="checkbox" name="lactoVegetarian" value={this.state.lactoVegetarian}
             checked={this.state.lactoVegetarian} onChange={this.handleInputChange} />Lacto vegetarian
          </label>
          <label>
            <input type="checkbox" name="ovoVegetarian" value={this.state.ovoVegetarian}
             checked={this.state.ovoVegetarian} onChange={this.handleInputChange} />Ovo vegetarian
          </label>
          <label>
            <input type="checkbox" name="pescetarian" value={this.state.pescetarian}
             checked={this.state.pescetarian} onChange={this.handleInputChange} />Pescetarian
          </label>
          <label>
            <input type="checkbox" name="vegan" value={this.state.vegan}
             checked={this.state.vegan} onChange={this.handleInputChange} />Vegan
          </label><br /><br />
          <label>
            <input type="checkbox" name="lactoOvoVeg" value={this.state.lactoOvoVeg}
             checked={this.state.lactoOvoVeg} onChange={this.handleInputChange} />Lacto-ovo vegetarian
          </label>
          <label>
            <input type="checkbox" name="paleo" value={this.state.paleo}
             checked={this.state.paleo} onChange={this.handleInputChange} />Paleo
          </label><br /><br />


          <button value="Search" onClick={this.handleSaveBtnClick} className="btn-all btn-save">Search</button>

        </form>

        <div>
          <ul>
          {recipesList && (
            recipesList.map(recipe => {
              let linkUrl = `http://www.yummly.co/recipe/${recipe.id}`
              let altStr = `${recipe.recipeName} photo`;
//              let imgUrl
              //let ingredientsStr = recipe.ingredients.join(', ')
              //console.log('recipe.ingredients: ' + recipe.ingredients)
              //console.log('ingredientsStr: ' + ingredientsStr)
              /* TODO helper to convert seconds to hours and minutes */
              //console.log('cook time sec: ' + recipe.totalTimeInSeconds)
              //let cookTimeMin = Math.ceil(recipe.totalTimeInSeconds / 60)
              // if (r.imageUrlsBySize) {
              //   imgUrl = r.imageUrlsBySize['90']
              // }

              let imgSmUrl = `${recipe.smallImageUrls[0]}` || '';
              //let imgMdUrl = `${recipe.img_url_lg}` || `${recipe.img_url_md}` || `${recipe.img_url_sm}`
              //let ingredientsStr = '';
              let ingredientsCnt = 0;
              if (recipe.ingredients) {
                //ingredientsStr = recipe.ingredients.join(', ');
                ingredientsCnt = recipe.ingredients.length;
              }

              return (
                <div className="recipe-wrapper-in">
                  <div className="recipe-div1">
                    {imgSmUrl.length && (
                      <a href={linkUrl} target="_blank">
                        <img src={imgSmUrl} alt={altStr}  className="recipe-img" />
                      </a>
                    )}
                  </div>
                  <div className="recipe-div2">
                    <p className="heading"><a href={linkUrl} target="_blank">{recipe.recipeName}</a></p>
                    <p><span className="subheading">Time</span> {this.formatTime(recipe.totalTimeInSeconds)}</p>
                    {ingredientsCnt > 0 && (
                      <details>
                        <summary><span className="subheading">{ingredientsCnt} Ingredients</span></summary>
                        <ul>
                          {recipe.ingredients.map(i => (
                            <li className="li-ingredient">{i}</li>
                          ))}
                        </ul>
                      </details>
                    )}
                  </div>
                </div>
              )
            })
          )}
          </ul>
        </div>

      </div>
    );
  }
}

function mapStateToProps(recipeSearch) {
  return { recipeSearch: recipeSearch }
}

export default connect(mapStateToProps)(RecipeSearch);
