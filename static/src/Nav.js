import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

import './App.css';

class Nav extends Component {

  render() {
    //const post = this.props.post
    return (
      <div className="header title">
        <div className="header-main">
          <h2><Link to="/" className="header-main">Seed to Soul</Link></h2>
        </div>
        <div className="header-contact">
          <p className="contact">
            <Link to="/recipes/current">Recommended Recipes</Link>
            <Link to="/recipes/search">Search Recipes</Link>
            <Link to="/settings/recipes">Settings</Link>
          </p>
        </div>
      </div>
    )
  }
}

export default Nav