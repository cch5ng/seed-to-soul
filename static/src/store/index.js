import { combineReducers } from 'redux'
import { settingsRecipes } from '../settings/SettingsRecipeReducers'
import { recipesList } from '../recipesList/RecipesListReducers'
import { recipeSearch } from '../recipeSearch/RecipeSearchReducers'
import { registration } from '../registration/RegistrationReducers'

export default combineReducers({
  settingsRecipes,
  recipesList,
  recipeSearch,
  registration
})
