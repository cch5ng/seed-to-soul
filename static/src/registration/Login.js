import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { postLogin } from '../registration/RegistrationActions'

class Login extends Component {

  state = {
    email: '',
    password: '',
    fireRedirect: false,
    isAuthenticated: this.props.registration && this.props.registration.isAuthenticated
      ? this.props.registration.registration.isAuthenticated : false
  }

  handleInputChange = this.handleInputChange.bind(this);
  handleSaveBtnClick = this.handleSaveBtnClick.bind(this);

  componentWillReceiveProps(nextProps) {
    if (nextProps.registration.isAuthenticated !== this.props.registration.isAuthenticated) {
      this.setState({triggerRender: !this.state.triggerRender,
        isAuthenticated: nextProps.registration.isAuthenticated
      })
    }
  }

  /*
    @param { onChange event }
    @return { none }; updates state
    Event handler for all inputs (except save button).
  */
  handleInputChange(ev) {
    const target = ev.target;
    let value;

    if (target.type === 'email' || target.type === 'text' || target.type === 'password') {
      value = target.value
    }
    const name=target.name;

    this.setState({
      [name]: value
    });
  }

  /*
    @param { onClick event }
    @return { none }
    Event handler for Save button. REFACTOR? this seems to be used different 
    places but the resulting 
  */
  handleSaveBtnClick(ev) {
    ev.preventDefault();

    let queryObj = {};
    let form = document.querySelector('.login')
    queryObj['email'] = this.state.email;
    // TODO this should be done over https ideally
    queryObj['password'] = this.state.password;
    this.props.postLogin(queryObj);
    this.setState({fireRedirect: true})
    form.reset();
  }

  // HELPERS

/* 
        {fireRedirect && (
          <Redirect to={"/"}/>
        )}
*/
// div-login

  render() {
    const { fireRedirect } = this.state;
    return (
      <div className="">
        {fireRedirect && this.state.isAuthenticated && (
          <Redirect to={"/recipes"}/>
        )}

        <h1>Login</h1>
        <Link to="/registration">Need an account?</Link>
        <form className="login">
          <input type="email" name="email" className="inp-email" required
             value={this.state.email} placeholder="Email"
             onChange={this.handleInputChange} />
          <input type="password" name="password" value={this.state.password}
            minlength="10" maxlength="72" placeholder="Password"
            onChange={this.handleInputChange} className="inp-password" required />
          <button value="Save" onClick={this.handleSaveBtnClick} className="btn-all btn-login">Login</button>
        </form>
      </div>
    );
  }
}

const mapDispatchToProps = {
  postLogin,
};

function mapStateToProps({ registration }) {
  return { registration }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
