import { REQUEST_POST_REGISTRATION, RECEIVE_POST_REGISTRATION,
         REQUEST_POST_LOGIN,
         RECEIVE_POST_LOGIN,
         LOGIN_FAILURE,
         RECEIVE_LOGOUT,
         LOGOUT_FAILURE} from '../registration/RegistrationActions';


// QUEUE ask whether login process affects changes to db at all

const initState = {
  isFetching: false,
  isAuthenticated: localStorage.getItem('id_token') ? true : false
}
// const initState = { authenticated: false }

// reducer gets auth status via localStorage
export function registration(state = initState, action) {
  switch(action.type) {
    case REQUEST_POST_REGISTRATION:
      return {
        ...state,
        retrievingRegistration: action.retrieving,
      }
    case RECEIVE_POST_REGISTRATION:
      return {
        ...state,
        email: action.token.email,
        isAuthenticated: action.token.authenticated,
        retrievingRegistration: action.retrieving,
      }

    case REQUEST_POST_LOGIN:
      return {
        ...state,
        retrievingLogin: action.retrieving,
        isAuthenticated: false,
        // user: action.creds
      }
    case RECEIVE_POST_LOGIN:
      return {
        ...state,
        //email: action.token.email,
        isAuthenticated: action.isAuthenticated, // token.authenticated,
        retrievingLogin: action.retrieving,
        errorMessage: '',
        token: action.token
      }
    case LOGIN_FAILURE:
      return {
        ...state,
        retrievingLogin: action.retrieving,
        isAuthenticated: action.isAuthenticated,
        errorMessage: action.message
      }
    case RECEIVE_LOGOUT:
      console.log('reducer for RECEIVE_LOGOUT')
      return {
        ...state,
        retrieving: false,
        isAuthenticated: false
      }


    // case REQUEST_POST_LOGIN:
    //   return {
    //     ...state,
    //     retrievingLogin: action.retrieving,
    //   }
    // case RECEIVE_POST_LOGIN:
    //   return {
    //     ...state,
    //     email: action.token.email,
    //     isAuthenticated: action.token.authenticated,
    //     retrievingLogin: action.retrieving,
    //   }
    default:
      return state
  }
}
