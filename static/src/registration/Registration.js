import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { postRegistration } from '../registration/RegistrationActions';

class Registration extends Component {

  state = {
    email: '',
    password: '',
    // passwordConfirm: '',
    phone: '',
    fireRedirect: false,
  }

  handleInputChange = this.handleInputChange.bind(this);
  handleSaveBtnClick = this.handleSaveBtnClick.bind(this);

  /*
    @param { onChange event }
    @return { none }; updates state
    Event handler for all inputs (except save button).
  */
  handleInputChange(ev) {
    const target = ev.target;
    let value;
    let phoneErr = '';

    if (target.type === 'text' || target.type === 'email' ||
        target.type === 'password' || target.type === 'tel') {
          value = target.value
    }
    const name=target.name;

    if (target.type === 'tel' || name === 'phone') {
      // check if the value translates to valid num
      // append the '+1'
      let phoneNum = parseInt(value, 10)
      if (isNaN(parseInt(value, 10))) {
        console.log('parseInt(value, 10): ' + parseInt(value, 10))
        console.log('isNaN(phoneNum): ' + isNaN(phoneNum))
        phoneErr = 'That is not a valid phone number.'
      } else if (value.length === 10) {
        value = '+1' + value;
        console.log('value: ' + value)
      }
    }

    this.setState({
      [name]: value,
      phoneErr
    });
  }

  /*
    @param { onClick event }
    @return { none }; triggers Redux store update/action which also triggers
    internal API call to update DB (User table)
    Event handler for Save button.
  */
  handleSaveBtnClick(ev) {
    ev.preventDefault();
    let queryObj = {};
    queryObj['email'] = this.state.email;

    // TODO validation on password matching
    queryObj['password'] = this.state.password;
    queryObj['phone'] = this.state.phone

    this.props.postRegistration(queryObj);
    this.clearRegForm();
    this.setState({fireRedirect: true})
  }

  // HELPERS
  clearRegForm() {
    this.setState({
      email: '',
      password: '',
      // remove to reduce complexity
      // passwordConfirm: '',
      phone: '',
    })
  }

/* remove for now to reduce complexity
          <input type="password" name="passwordConfirm"
            value={this.state.passwordConfirm} minlength="10" maxlength="72"
            placeholder="Password confirmation"
            onChange={this.handleInputChange} required />
          <br /><br />
*/
  render() {
    const { fireRedirect } = this.state;
    return (
      <div className="">
        <h1>Registration</h1>
        <form className="regForm">
          <input type="email" name="email" className="" required
           value={this.state.email} placeholder="Email"
           onChange={this.handleInputChange} />
          <br /><br />
          <input type="password" name="password" value={this.state.password}
            minlength="10" maxlength="72"
            placeholder="Password, 10 letter min"
            onChange={this.handleInputChange} required />
          <br /><br />
          <input type="tel" name="phone" value={this.state.phone}
            placeholder="Mobile phone (notifications)"
            onChange={this.handleInputChange} />
          <br /><br />
          <button value="Save" onClick={this.handleSaveBtnClick} className="btn-all btn-save">Save</button>
        </form>
        {fireRedirect && (
          <Redirect to={"/settings/recipes"}/>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = {
  postRegistration,
};

export default connect(null, mapDispatchToProps)(Registration);
