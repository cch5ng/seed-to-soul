
const API_POST_REGISTRATION = 'http://localhost:5000/api/registration'
const API_POST_LOGIN = 'http://localhost:5000/api/login'
export const REQUEST_POST_REGISTRATION = 'REQUEST_POST_REGISTRATION';
export const RECEIVE_POST_REGISTRATION = 'RECEIVE_POST_REGISTRATION';
export const REQUEST_POST_LOGIN = 'REQUEST_POST_LOGIN';
export const RECEIVE_POST_LOGIN = 'RECEIVE_POST_LOGIN';
export const LOGIN_FAILURE = 'LOGIN_FAILURE'
export const REQUEST_LOGOUT = 'REQUEST_LOGOUT';
export const RECEIVE_LOGOUT = 'RECEIVE_LOGOUT';
export const LOGOUT_FAILURE = 'LOGOUT_FAILURE'


export function requestPostRegistration() {
  return {
    type: REQUEST_POST_REGISTRATION,
    retrieving: true
  }
}

export function receivePostRegistration(token) {
  document.cookie = `authenticated=${token.email}; Path=/;`;
  return {
    type: RECEIVE_POST_REGISTRATION,
    token,
    retrieving: false
  }
}

export const postRegistration = (queryObj) => dispatch => {
  dispatch(requestPostRegistration());
  return fetch(API_POST_REGISTRATION, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json',
    },
    body: JSON.stringify(queryObj)
  })
    .then(response => response.json() )
    .then(json => {
      dispatch(receivePostRegistration(json))
    })
    .catch(function(err) {
      console.log('fetch err: ' + err.message)
    });
}

export function requestPostLogin(creds) {
  return {
    type: REQUEST_POST_LOGIN,
    retrieving: true,
    isAuthenticated: false,
    //creds
  }
}

// export function requestPostLogin() {
//   return {
//     type: REQUEST_POST_LOGIN,
//     retrieving: true,
//   }
// }

export function receivePostLogin(json) {
  // document.cookie = `authenticated=${token.email}; Path=/;`;
  return {
    type: RECEIVE_POST_LOGIN,
    retrieving: false,
    isAuthenticated: true,
    token: json.token
  }
}

// export function receivePostLogin(token) {
//   document.cookie = `authenticated=${token.email}; Path=/;`;
//   return {
//     type: RECEIVE_POST_LOGIN,
//     token,
//     retrieving: false
//   }
// }

export function loginError(message) {
  return {
    type: LOGIN_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    message
  }
}

export const postLogin = (queryObj) => dispatch => {
  dispatch(requestPostLogin()); //creds
  return fetch(API_POST_LOGIN, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json',
    },
    body: JSON.stringify(queryObj)
  })
    .then(response => response.json() )
    .then(json => {
      localStorage.setItem('access_token', json.token);
      dispatch(receivePostLogin(json));
    })
    .catch(function(err) {
      console.log('fetch err: ' + err.message)
    });
}

// export const postLogin = (queryObj) => dispatch => {
//   dispatch(requestPostLogin());
//   return fetch(API_POST_LOGIN, {
//     method: 'POST',
//     headers: {
//       "Content-Type": 'application/json',
//     },
//     body: JSON.stringify(queryObj)
//   })
//     .then(response => response.json() )
//     .then(json => {
//       dispatch(receivePostLogin(json))
//     })
//     .catch(function(err) {
//       console.log('fetch err: ' + err.message)
//     });
// }

export function requestLogout() {
  return {
    type: REQUEST_LOGOUT,
    retrieving: true,
    isAuthenticated: true
  }
}

export function receiveLogout() {
  console.log('gets to receiveLogout');
  return {
    type: RECEIVE_LOGOUT,
    retrieving: false,
    isAuthenticated: false
  }
}


// Logs the user out
export function logoutUser() {
  return dispatch => {
    dispatch(requestLogout())
    // localStorage.removeItem('id_token')
    localStorage.removeItem('access_token')
    dispatch(receiveLogout())
  }
}
