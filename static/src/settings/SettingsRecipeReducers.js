import { REQUEST_RECIPES, RECEIVE_RECIPES, REQUEST_UPDATE_RECIPE_SETTINGS,
  RECEIVE_UPDATE_RECIPE_SETTINGS, REQUEST_RECIPE_SETTINGS,
  RECEIVE_RECIPE_SETTINGS } from '../settings/SettingsRecipeActions';

const initState = { settingsRecipes: {
                      maxCarbs: 0,
                      notifyDay: 'Saturday'
                    },
                    recipes: {}
                  }

export function settingsRecipes(state = initState, action) {
  switch(action.type) {
    case REQUEST_RECIPES:
      return {
        ...state,
        retrievingRecipesList: action.retrieving,
      }
    case RECEIVE_RECIPES:
      let recipesObj = {}
      action.recipes.forEach(recipe => {
        recipesObj[recipe.id] = recipe
      })
      return {
        ...state,
        recipes: recipesObj,
        retrievingRecipesList: action.retrieving,
      }
    case REQUEST_RECIPE_SETTINGS:
      return {
        ...state,
        retrievingRecipeSettings: action.retrieving,
      }
    case RECEIVE_RECIPE_SETTINGS:
      return {
        ...state,
        settingsRecipes: action.settings,
        retrievingRecipeSettings: action.retrieving,
      }
    default:
      return state
  }
}
