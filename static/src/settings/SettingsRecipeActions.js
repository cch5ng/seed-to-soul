
const API_GET_RECIPE_SUMMARY_LIST = 'http://localhost:5000/api/recipe_search'
const API_GET_RECIPE_SETTINGS = 'http://localhost:5000/api/recipe_settings'
const API_POST_RECIPE_SETTINGS = 'http://localhost:5000/api/update_recipe_settings'
export const REQUEST_RECIPES = 'REQUEST_RECIPES';
export const RECEIVE_RECIPES = 'RECEIVE_RECIPES';
export const REQUEST_RECIPE_SETTINGS = 'REQUEST_RECIPE_SETTINGS';
export const RECEIVE_RECIPE_SETTINGS = 'RECEIVE_RECIPE_SETTINGS';
export const REQUEST_UPDATE_RECIPE_SETTINGS = 'REQUEST_UPDATE_RECIPE_SETTINGS';
export const RECEIVE_UPDATE_RECIPE_SETTINGS = 'RECEIVE_UPDATE_RECIPE_SETTINGS';

export function requestRecipeSettings() {
  return {
    type: REQUEST_RECIPE_SETTINGS,
    retrieving: true
  }
}

export function receiveRecipeSettings(settings) {
  return {
    type: RECEIVE_RECIPE_SETTINGS,
    settings,
    retrieving: false
  }
}

export const getRecipeSettings = () => dispatch => {
  let accessToken = localStorage.getItem('access_token');
  dispatch(requestRecipeSettings());
  return fetch(API_GET_RECIPE_SETTINGS, {
    method: 'POST',
    headers: {
      "Authorization": `JWT ${accessToken}`
    }
  })
    .then(response => response.json() )
    .then(json => dispatch(receiveRecipeSettings(json)))
    .catch(function(err) {
      console.log('fetch err: ' + err.message)
    });
}

export function requestUpdateRecipeSettings() {
  return {
    type: REQUEST_UPDATE_RECIPE_SETTINGS,
    retrieving: true
  }
}

export function receiveUpdateRecipeSettings(recipeSettings) {
  return {
    type: RECEIVE_UPDATE_RECIPE_SETTINGS,
    recipeSettings,
    retrieving: false
  }
}


export const updateRecipeSettings = (queryObj) => dispatch => {
  let accessToken = localStorage.getItem('access_token');
  dispatch(requestUpdateRecipeSettings());
  return fetch(API_POST_RECIPE_SETTINGS, {
    method: 'POST',
    headers: {
      "Authorization": `JWT ${accessToken}`,
      "Content-Type": 'application/json',
    },
    body: JSON.stringify(queryObj)
  })
    .then(response => response.json() )
    .then(json => {
      dispatch(receiveUpdateRecipeSettings(json))
    })
    .catch(function(err) {
    });
}

export function requestRecipes() {
  return {
    type: REQUEST_RECIPES,
    retrieving: true
  }
}

export function receiveRecipes(result) {
  let recipes = result.matches
  return {
    type: RECEIVE_RECIPES,
    recipes,
    retrieving: false
  }
}

export const fetchRecipes = (queryObj) => dispatch => {
  dispatch(requestRecipes());
  return fetch(API_GET_RECIPE_SUMMARY_LIST, {
    method: 'POST',
    headers: {
      "Content-Type": 'application/json',
    },
    body: JSON.stringify(queryObj)
  })
    .then(response => response.json())
    .then(json => dispatch(receiveRecipes(json)))
    .catch(function(err) {
    });
}
