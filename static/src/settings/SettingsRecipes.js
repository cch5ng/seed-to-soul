import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Select from 'react-select';
import VirtualizedSelect from 'react-virtualized-select';
import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import { fetchRecipes, updateRecipeSettings, getRecipeSettings } from '../settings/SettingsRecipeActions'

class SettingsRecipes extends Component {
  state = {
    maxCarbs: 0,
    maxSugar: 0,
    american: false,
    italian: false,
    mexican: false,
    southwestern: false,
    asian: false,
    indian: false,
    chinese: false,
    thai: false,
    japanese: false,
    hawaiian: false,
    mediterranean: false,
    greek: false,
    cuban: false,
    dislikeIngredients: '',
    notifyDay: 'Saturday',
    Gluten: false,
    Peanut: false,
    Seafood: false,
    Sesame: false,
    Soy: false,
    Dairy: false,
    Egg: false,
    Sulfite: false,
    TreeNut: false,
    Wheat: false,
    selectedOption: '',
    excludedIngredients: [],
  }

  handleInputChange = this.handleInputChange.bind(this);
  handleSaveBtnClick = this.handleSaveBtnClick.bind(this);
  diffCuisinesSettingsToState = this.diffCuisinesSettingsToState.bind(this);
  handleChange = this.handleChange.bind(this);

  componentDidMount() {
    this.props.dispatch(getRecipeSettings());
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.settingsRecipes.settingsRecipes) {
      const dbAllergyToJsAllergy = { "Gluten-Free": "Gluten",
                                     "Peanut-Free": "Peanut",
                                     "Seafood-Free": "Seafood",
                                     "Sesame-Free": "Sesame",
                                     "Soy-Free": "Soy",
                                     "Dairy-Free": "Dairy",
                                     "Egg-Free": "Egg",
                                     "Sulfite-Free": "Sulfite",
                                     "Tree Nut-Free": "TreeNut",
                                     "Wheat-Free": "Wheat"};

      let newState = {}
      let newCuisinesState = {}
      let newAllergyState = {}
      let newExcludedIngredientsState = {}
      if (this.state.maxCarbs !== nextProps.settingsRecipes.settingsRecipes.maxCarbs) {
        newState.maxCarbs = nextProps.settingsRecipes.settingsRecipes.maxCarbs
      }
      if (this.state.maxSugar !== nextProps.settingsRecipes.settingsRecipes.maxSugar) {
        newState.maxSugar = nextProps.settingsRecipes.settingsRecipes.maxSugar
      }
      if (this.state.notifyDay !== nextProps.settingsRecipes.settingsRecipes.notifyDay) {
        newState.notifyDay = nextProps.settingsRecipes.settingsRecipes.notifyDay
      }

      console.log('nextProps.settingsRecipes.settingsRecipes.cuisines: ' + nextProps.settingsRecipes.settingsRecipes.cuisines)
      if (nextProps.settingsRecipes.settingsRecipes.cuisines) {
        newCuisinesState = this.diffCuisinesSettingsToState(nextProps.settingsRecipes.settingsRecipes.cuisines) || {}
      }

      if (nextProps.settingsRecipes.settingsRecipes.allergies) {
        Object.keys(dbAllergyToJsAllergy).forEach(a => {
          if (nextProps.settingsRecipes.settingsRecipes.allergies.indexOf(a) > -1) {
            newAllergyState[dbAllergyToJsAllergy[a]] = true;
          } else {
            newAllergyState[dbAllergyToJsAllergy[a]] = false;
          }
        })
      }

      if (nextProps.settingsRecipes.settingsRecipes.excludedIngredients) {
        let excludedIngredientsList = [];
        nextProps.settingsRecipes.settingsRecipes.excludedIngredients.forEach(ing => {
          let innerExcludedIngredient = {};
          innerExcludedIngredient['label'] = ing;
          innerExcludedIngredient['value'] = ing;
          excludedIngredientsList.push(innerExcludedIngredient);
        });
        newExcludedIngredientsState['excludedIngredients'] = excludedIngredientsList
      }

      this.setState({...newState, ...newCuisinesState, ...newAllergyState, ...newExcludedIngredientsState})
    }
  }

  /*
    @param { onChange event }
    @return { none }; updates state
    Event handler for all inputs (except save button). Updates component state
    following React controlled form component pattern. Dependent on setting input
    name attribute and referencing this function as the onChange attribute handler.
    Ref pattern: https://reactjs.org/docs/forms.html#handling-multiple-inputs
    Previously I was writing a unique input change handler per form control but
    the documented pattern feels cleaner.
  */
  handleInputChange(ev) {
    const target = ev.target;
    let value;

    if (target.type === 'checkbox') {
      value = target.checked;
    }
    if (target.type === 'number' || target.type === 'text' || target.type === 'radio') {
      value = target.value;
    }
    const name=target.name;

    this.setState({
      [name]: value
    });
  }

  /*
   * @param
   * @result
   * Event handler for Excluded Ingredients input control. When user selects
   * an item from auto-suggest list, it will be added to comp state (array of
   * objects).
   */
  handleChange(selectedOption) {
    this.setState({ excludedIngredients: selectedOption,});
  }

  /*
    @param { onClick event }
    @return { none }; triggers Redux store update/action which also triggers
    internal API call to update DB
    Event handler for Save button. Uses component state
    following React controlled form component pattern. Dependent on setting input
    onClick attribute and referencing this function as the handler.
  */
  handleSaveBtnClick(ev) {
    ev.preventDefault();
    const ALL_YUMMLY_CUISINES = ["asian",
                                 "american",
                                 "italian",
                                 "mexican",
                                 "southwestern",
                                 "indian",
                                 "chinese",
                                 "thai",
                                 "japanese",
                                 "hawaiian",
                                 "mediterranean",
                                 "greek",
                                 "cuban",];
    const JS_ALLERGY_TO_YUMMLY = {"Gluten": "Gluten-Free",
                               "Peanut": "Peanut-Free",
                               "Seafood": "Seafood-Free",
                               "Sesame": "Sesame-Free",
                               "Soy": "Soy-Free",
                               "Dairy": "Dairy-Free",
                               "Egg": "Egg-Free",
                               "Sulfite": "Sulfite-Free",
                               "TreeNut": "Tree Nut-Free", //exception
                               "Wheat": "Wheat-Free"}
    let queryObj = {};
    let cuisineList = [];
    let allergiesList = [];
    let excludedIngredientsList;
    let maxCarbs = this.state.maxCarbs;
    let maxSugar = this.state.maxSugar;
    let notifyDay = this.state.notifyDay;

    ALL_YUMMLY_CUISINES.forEach(c => {
      if (this.state[c] === true) {
        cuisineList.push(c)
      }
    })

    Object.keys(JS_ALLERGY_TO_YUMMLY).forEach(a => {
      if (this.state[a]) {
        allergiesList.push(JS_ALLERGY_TO_YUMMLY[a])
      }
    })

    excludedIngredientsList = this.state.excludedIngredients.map((ing) => {
      return ing.value;
    });

    queryObj['maxCarbs'] = maxCarbs;
    queryObj['maxSugar'] = maxSugar;
    queryObj['cuisine'] = cuisineList;
    queryObj['notifyDay'] = notifyDay;
    queryObj['allergiesList'] = allergiesList;
    queryObj['excludedIngredientsList'] = excludedIngredientsList;
    this.props.dispatch(updateRecipeSettings(queryObj));
  }

  // HELPERS

  /*
   * @param {propsCuisinesAr} - array of cuisines saved in db for recipe settings
   * (of cur user)
   * Given cuisines array, performs a diff between all possible cuisines vs
   * user's saved cuisines. Returns an object with object format needed to update
   * component state for the field values.
   */
  diffCuisinesSettingsToState(propsCuisinesAr) {
    const ALL_YUMMLY_CUISINES = ["asian",
                                 "american",
                                 "italian",
                                 "mexican",
                                 "southwestern",
                                 "indian",
                                 "chinese",
                                 "thai",
                                 "japanese",
                                 "hawaiian",
                                 "mediterranean",
                                 "greek",
                                 "cuban",];

    let newCuisinesState = {};

    ALL_YUMMLY_CUISINES.forEach(c => {
      if (propsCuisinesAr.indexOf(c) > -1) {
        newCuisinesState[c] = true;
      } else {
        newCuisinesState[c] = false;
      }
    })

    return newCuisinesState;
  }

  /* 
   * @param
   * @return
   * Used by react-select (auto-suggest component) to fetch the list of
   * supported ingredients to exclude (for yummly search api query).
   *
   */
  getOptions() {
    return fetch('/api/recipe_search/ingredients.json')
      .then((resp) => {
        return resp.json();
      }).then((json) => {
        return { options: json };
      })
  }

  render() {
    const { selectedOption, excludedIngredients } = this.state;
    // QUEUE what does this mean?
    // same as if (selectedOption) { value = selectedOption.value }
    const value = selectedOption && selectedOption.value;

    return (
      <div className="">
        <h1>Recipe Preferences</h1>
        <form className="">
          <h2 className="h2-preference">Recipe Notification</h2>
          <div className="flex-row-notification">
            <div>
              <input type="radio" name="notifyDay" value="Saturday" onChange={this.handleInputChange}
                checked={"Saturday" === this.state.notifyDay} /> Saturday
              <input type="radio" name="notifyDay" value="Sunday" onChange={this.handleInputChange}
                checked={"Sunday" === this.state.notifyDay} /> Sunday
              <input type="radio" name="notifyDay" value="Monday" onChange={this.handleInputChange}
                checked={"Monday" === this.state.notifyDay} /> Monday
              <input type="radio" name="notifyDay" value="Tuesday" onChange={this.handleInputChange}
                checked={"Tuesday" === this.state.notifyDay} /> Tuesday
            </div>
            <div>
              <input type="radio" name="notifyDay" value="Wednesday" onChange={this.handleInputChange}
                checked={"Wednesday" === this.state.notifyDay} /> Wednesday
              <input type="radio" name="notifyDay" value="Thursday" onChange={this.handleInputChange}
                checked={"Thursday" === this.state.notifyDay} /> Thursday
              <input type="radio" name="notifyDay" value="Friday" onChange={this.handleInputChange}
                checked={"Friday" === this.state.notifyDay} /> Friday
            </div>
          </div>

          <h2 className="h2-preference">Nutrition</h2>
          <label>
            Max Sugar (g)
            <input type="number" name="maxSugar" className="" 
             value={this.state.maxSugar} min="0"
             onChange={this.handleInputChange} />
          </label><br /><br />
          <label>
            Max Carbs (g)
            <input type="number" name="maxCarbs" className="" 
             value={this.state.maxCarbs} min="0"
             onChange={this.handleInputChange} />
          </label>
          <h2 className="h2-preference">Ethnic Cuisine Preferences</h2>
          <label>
            <input type="checkbox" name="asian" value={this.state.asian}
             checked={this.state.asian} onChange={this.handleInputChange} />Asian
          </label>
          <label>
            <input type="checkbox" name="chinese" value={this.state.chinese}
             checked={this.state.chinese} onChange={this.handleInputChange} />Chinese
          </label>
          <label>
            <input type="checkbox" name="japanese" value={this.state.japanese}
             checked={this.state.japanese} onChange={this.handleInputChange} />Japanese
          </label>
          <label>
            <input type="checkbox" name="hawaiian" value={this.state.hawaiian}
             checked={this.state.hawaiian} onChange={this.handleInputChange} />Hawaiian
          </label><br /><br />

          <label>
            <input type="checkbox" name="mexican"  value={this.state.mexican}
             checked={this.state.mexican} onChange={this.handleInputChange} />Mexican
          </label>
          <label>
            <input type="checkbox" name="italian"  value={this.state.italian}
             checked={this.state.italian} onChange={this.handleInputChange} />Italian
          </label>
          <label>
            <input type="checkbox" name="mediterranean"  value={this.state.mediterranean}
               checked={this.state.mediterranean} onChange={this.handleInputChange} />Mediterranean
          </label>

          <details className="details-cuisines">
            <summary><span class="subheading">More Cuisines</span>
            </summary>
            <label>
              <input type="checkbox" name="thai" value={this.state.thai}
               checked={this.state.thai} onChange={this.handleInputChange} />Thai
            </label>
            <label>
              <input type="checkbox" name="indian" value={this.state.indian}
               checked={this.state.indian} onChange={this.handleInputChange} />Indian
            </label><br /><br />
            <label>
              <input type="checkbox" name="southwestern"  value={this.state.southwestern}
               checked={this.state.southwestern} onChange={this.handleInputChange} />Southwestern
            </label>
            <label>
              <input type="checkbox" name="cuban"  value={this.state.cuban}
               checked={this.state.cuban} onChange={this.handleInputChange} />Cuban
            </label><br /><br />
            <label>
              <input type="checkbox" name="american"  value={this.state.american}
               checked={this.state.american} onChange={this.handleInputChange} />American
            </label>
            <label>
              <input type="checkbox" name="greek"  value={this.state.greek}
               checked={this.state.greek} onChange={this.handleInputChange} />Greek
            </label>
          </details>

          <h2 className="h2-preference">Allergies</h2>
          <label>
            <input type="checkbox" name="Gluten" value={this.state.Gluten}
             checked={this.state.Gluten} onChange={this.handleInputChange} />Gluten
          </label>
          <label>
            <input type="checkbox" name="Peanut"  value={this.state.Peanut}
             checked={this.state.Peanut} onChange={this.handleInputChange} />Peanut
          </label>
          <label>
            <input type="checkbox" name="Seafood"  value={this.state.Seafood}
             checked={this.state.Seafood} onChange={this.handleInputChange} />Seafood
          </label>
          <label>
            <input type="checkbox" name="Soy"  value={this.state.Soy}
             checked={this.state.Soy} onChange={this.handleInputChange} />Soy
          </label>
          <label>
            <input type="checkbox" name="Dairy"  value={this.state.Dairy}
             checked={this.state.Dairy} onChange={this.handleInputChange} />Dairy
          </label>

          <details className="details-allergies">
            <summary><span class="subheading">More Allergies</span>
            </summary>
            <label>
              <input type="checkbox" name="Wheat"  value={this.state.Wheat}
               checked={this.state.Wheat} onChange={this.handleInputChange} />Wheat
            </label>
            <label>
              <input type="checkbox" name="Egg"  value={this.state.Egg}
               checked={this.state.Egg} onChange={this.handleInputChange} />Egg
            </label>
            <label>
              <input type="checkbox" name="Sesame"  value={this.state.Sesame}
               checked={this.state.Sesame} onChange={this.handleInputChange} />Sesame
            </label>
            <label>
              <input type="checkbox" name="Sulfite"  value={this.state.Sulfite}
               checked={this.state.Sulfite} onChange={this.handleInputChange} />Sulfite
            </label>
            <label>
              <input type="checkbox" name="TreeNut"  value={this.state.TreeNut}
               checked={this.state.TreeNut} onChange={this.handleInputChange} />Tree Nut
            </label>
          </details>

          <h2 className="h2-preference">Excluded Ingredients</h2>
          <VirtualizedSelect
            async
            loadOptions={this.getOptions}
            multi
            labelKey='label'
            name="excluded_ingredients"
            value={excludedIngredients}
            minimumInput={1}
            onChange={this.handleChange}
            delimiter=','
            joinValues='true'
          />

          <button value="Save" onClick={this.handleSaveBtnClick}
           className="btn-all btn-settings-save btn-save">Save</button>

        </form>

      </div>
    );
  }
}

function mapStateToProps(settingsRecipes) {
  return { settingsRecipes: settingsRecipes.settingsRecipes }
}

export default connect(mapStateToProps)(SettingsRecipes);
