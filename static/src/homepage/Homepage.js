import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch
} from 'react-router-dom';
import { connect } from 'react-redux';
import './Homepage.css';
import { fetchRecipes } from '../settings/SettingsRecipeActions'

class Homepage extends Component {

  handleYummlyBtn = this.handleYummlyBtn.bind(this)
  handleTwilioBtn = this.handleTwilioBtn.bind(this)

  handleYummlyBtn(ev) {
    console.log('Yummly test btn clicked');
    this.props.dispatch(fetchRecipes({}));

  }

  handleTwilioBtn(ev) {
    console.log('Twilio test btn clicked');
  }


// TODO move into 2nd button onClick={this.handleTwilioBtn}
  render() {
    return (
      <div className="">
          <h1 className="">Homepage content</h1>
          <h1 className="">MVP tests</h1>

          <button onClick={this.handleYummlyBtn}>Test Yummly Search</button><br /><br /><br />
          <p><Link to="/settings/recipes">Link to current recipe recommendations</Link></p>
          <button onClick={this.handleTwilioBtn}>Test Twilio Notification</button>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { }
}

export default connect(mapStateToProps)(Homepage);
// export default Homepage;
