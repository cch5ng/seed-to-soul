import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch
} from 'react-router-dom';
import { connect } from 'react-redux';
import './Homepage.css';
import { fetchRecipes } from '../settings/SettingsRecipeActions'

class Test extends Component {

  handleYummlyBtn = this.handleYummlyBtn.bind(this)
  handleTwilioBtn = this.handleTwilioBtn.bind(this)

  handleYummlyBtn(ev) {
    let email = document.cookie.replace(/(?:(?:^|.*;\s*)authenticated\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    console.log('Yummly test btn clicked');
    this.props.dispatch(fetchRecipes({'email': email}));

  }

  handleTwilioBtn(ev) {
    console.log('Twilio test btn clicked');
  }


// TODO move into 2nd button onClick={this.handleTwilioBtn}
  render() {
    return (
      <div className="">
          <h1 className="">Test content</h1>
          <button onClick={this.handleYummlyBtn}>Test Yummly Search</button><br /><br /><br />
          <p><Link to="/settings/recipes">Link to current recipe recommendations</Link></p>
          <button onClick={this.handleTwilioBtn}>Test Twilio Notification</button>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { }
}

export default connect(mapStateToProps)(Test);
