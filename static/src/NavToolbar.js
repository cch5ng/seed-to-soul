import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import Login from './registration/Login';
import { logoutUser } from './registration/RegistrationActions';

import './App.css';

class NavToolbar extends Component {

  state = {
    triggerRender: true,
    // MENTOR: verify if this best way to ensure view up to date
    isAuthenticated: this.props.registration && this.props.registration.isAuthenticated
      ? this.props.registration.registration.isAuthenticated : false
  };

  handleLogoutBtnClick = this.handleLogoutBtnClick.bind(this);

  componentWillReceiveProps(nextProps) {
    if (nextProps.registration.isAuthenticated !== this.props.registration.isAuthenticated) {
      this.setState({triggerRender: !this.state.triggerRender,
        isAuthenticated: nextProps.registration.isAuthenticated
      })
    }
  }

  handleLogoutBtnClick(ev) {
    this.props.logoutUser();
  }

  render() {
    let isAuthenticated = this.state.isAuthenticated;

    return (
      <Navbar collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/" className="header-main">Seed to Soul</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight isAuthenticated={isAuthenticated}>
            <Navbar.Text><Link className="contact" to="/recipes/current">Recommended Recipes</Link></Navbar.Text>
            <Navbar.Text><Link className="contact" to="/recipes/search">Search Recipes</Link></Navbar.Text>
            <Navbar.Text><Link className="contact" to="/settings/recipes">Settings</Link></Navbar.Text>
            { !isAuthenticated && (<Navbar.Text><Link className="contact" to="/login">Log In</Link></Navbar.Text>)}
            { isAuthenticated && (<Navbar.Text><Link className="contact" to="/" onClick={this.handleLogoutBtnClick}>Logout</Link></Navbar.Text>) }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

const mapDispatchToProps = {
  logoutUser
};

function mapStateToProps({ registration }) {
  return {
    registration
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NavToolbar);
