import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch
} from 'react-router-dom';
import { connect } from 'react-redux';
import Homepage from './homepage/Homepage';
import SettingsRecipes from './settings/SettingsRecipes';
import RecipesList from './recipesList/RecipesList';
import RecipeSearch from './recipeSearch/RecipeSearch';
import Registration from './registration/Registration';
import Login from './registration/Login';
import NavToolbar from './NavToolbar';
import { postRecipeUpdate } from './recipesList/RecipesListActions'
import Test from './homepage/Test';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  recipeBtnClickHandler = this.recipeBtnClickHandler.bind(this)
  recipeUserRatingChangeHandler = this.recipeUserRatingChangeHandler.bind(this)

  /*
   *
   *
   *
   */
  recipeBtnClickHandler(btnId, btnState) {
    // console.log('btnId: ' + btnId)
    let idAr = btnId.split('&&')
    let btnType = idAr[0]
    let recipeId = idAr[1]
    let recipeState = {}
    // console.log('btnType: ' + btnType)
    let email = document.cookie.replace(/(?:(?:^|.*;\s*)authenticated\s*\=\s*([^;]*).*$)|^.*$/, "$1");

    switch(btnType) {
      case 'btnSave':
        console.log('save btn')
        // need to update saved dynamically
        recipeState = {'saved': btnState, 'recipeId': recipeId, 'email': email}
        this.props.dispatch(postRecipeUpdate(recipeState))
        return

      // case 'btnHide':
      //   console.log('hide btn')
      //   // need to update saved dynamically
      //   recipeState = {'hidden': btnState, 'recipeId': recipeId}
      //   this.props.dispatch(postRecipeUpdate(recipeState))
      //   return
      default:
        return
    }
  }

  /*
   *
   *
   *
   */
  recipeUserRatingChangeHandler(inputId, rating) {
    // REFACTOR with ABOVE
    console.log('inputId: ' + inputId)
    console.log('rating: ' + rating)
    let idAr = inputId.split('&&')
    //let inputType = idAr[0]
    let recipeId = idAr[1]
    let email = document.cookie.replace(/(?:(?:^|.*;\s*)authenticated\s*\=\s*([^;]*).*$)|^.*$/, "$1");

    let ratingState = {'userRating': rating, 'recipeId': recipeId, 'email': email}
    this.props.dispatch(postRecipeUpdate(ratingState))
  }

  // HELPERS



  render() {
/*
<RecipesList recipeBtnClickHandler={this.recipeBtnClickHandler}
                  recipeUserRatingChangeHandler={this.recipeUserRatingChangeHandler} />
*/

    return (
      <Router>
        <div className="App">
          <NavToolbar />
          <main>
            <Switch>
              <Route exact path="/" render={() => <Login /> }
              />
              <Route exact path="/login" component={Login} />
              <Route exact path="/registration" component={Registration} />
              <Route exact path="/recipes/search"
                render={() => <RecipeSearch recipeBtnClickHandler={this.recipeBtnClickHandler}
                  recipeUserRatingChangeHandler={this.recipeUserRatingChangeHandler}
                />} />
              <Route path="/recipes"
                render={() => <RecipesList recipeBtnClickHandler={this.recipeBtnClickHandler}
                  recipeUserRatingChangeHandler={this.recipeUserRatingChangeHandler}
                />} />
              <Route path="/settings/recipes" component={SettingsRecipes} />
              <Route path="/test" component={Test} />
            </Switch>
          </main>
          <footer>
            <p className="footer-p">Recipe search powered by <a href='http://www.yummly.co/recipes' target="_blank"><img alt='Yummly' src='https://static.yummly.co/api-logo.png'/></a></p>
            <p className="footer-p">Created with ☕ and 🍪 by <a href='http://github.com/cch5ng/seed-to-soul' target="_blank" className='footer-a'>cch5ng | source</a></p>
            <p className="footer-p">Thanks to Hackbright Academy</p>
          </footer>
        </div>
      </Router>
    );
  }
}

export default App;
