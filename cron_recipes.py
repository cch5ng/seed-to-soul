#!/usr/bin/python
# script just to test out using crontab for scheduled jobs
from server import (search_recipe_list, app) #, twilio_sms
from model import (connect_to_db, db, Recipe_Settings, RecipeSettingsCuisine,
                   User)
from datetime import date
import sendgrid
import os
from sendgrid.helpers.mail import *

# setup for db access
connect_to_db(app)


def get_users_to_notify_today():
    """Checks today's weekday value and searches db for matches in
        User.notify_day. Returns a list of user emails.
    """

    cur_day = date.today().strftime("%A")
    rs_today_recs = db.session.query(Recipe_Settings).filter(Recipe_Settings.notification_day == cur_day).all()
    user_emails = []
    for a in rs_today_recs:
        user_rec = User.query.get(a.user_id)
        user_emails.append(user_rec.email)

    return user_emails

def email_user(email):
    """
    """

    sg = sendgrid.SendGridAPIClient(apikey=os.environ['SENDGRID_API_KEY'])
    from_email = Email("seedtosoul@example.com")
    to_email = Email(email)
    subject = "Your weekly recipes are ready"
    content = Content("text/plain", "Here you go: http://localhost:5000/recipes/current")
    mail = Mail(from_email, subject, to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())
    print(response.status_code)
    print(response.body)
    print(response.headers)


emails = get_users_to_notify_today()

for e in emails:
    search_recipe_list(e)
    email_user(e)
    #twilio_sms(e)
