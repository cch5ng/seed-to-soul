"""Models and database functions for db."""
# prerequisite: assume a db called seedsoul already exists

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


# Part 1: Compose ORM

class User(db.Model):
    """User model. TODO make phone, unique=True. Temp disable for testing.
    """

    __tablename__ = "users"

    user_id = db.Column(db.Integer,
                        primary_key=True,
                        autoincrement=True,
                        unique=True)
    email = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(100), nullable=False, unique=True)
    phone = db.Column(db.String(20), nullable=False)

    def __repr__(self):
        """Show info about user"""

        return "<User id={user_id} email={email} >".format(
            user_id=self.user_id, email=self.email)


class Recipe_Settings(db.Model):
    """Recipe Settings model."""

    __tablename__ = "recipe_settings"

    recipe_settings_id = db.Column(db.Integer,
                                   primary_key=True,
                                   autoincrement=True,
                                   unique=True)
    max_carbs = db.Column(db.Integer, nullable=True)
    max_sugar = db.Column(db.Integer, nullable=True)
    # may be broken to separate table
    allergies = db.Column(db.String(300), nullable=True)
    ingredients_exclude = db.Column(db.String(300), nullable=True)
    notification_day = db.Column(db.String(10), nullable=True)
    # QUESTION is this correct if I just want a general time like 5:00pm
    notification_time = db.Column(db.DateTime, nullable=True)

    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'))


    # def __repr__(self):
    """Show info about animal"""

    #     return "<Recipe_Settings id={recipe_settings_id} max_carbs={max_carbs} >".format(recipe_settings_id=self.recipe_settings_id, max_carbs=self.max_carbs)


class Cuisine_Type(db.Model):
    """Cuisine Type subtable, relating to Recipe_Settings table. Contains a
        limited list of options from Yummly.
        List of options supported by Yummly (go to page and find
        "supported cuisines":
        https://developer.yummly.com/documentation
    """

    __tablename__ = "cuisine_types"

    cuisine_id = db.Column(db.Integer,
                           primary_key=True,
                           autoincrement=True)

    # TODO refactor PK should be cuisine_name
    cuisine_name = db.Column(db.String(30),
                             nullable=False)
    search_value = db.Column(db.String(30),
                             nullable=False)

    def __repr__(self):
        """Show info about ethnic cuisine"""

        return "<Ethnic_Cuisine cuisine_name={cuisine_name} search_value={search_value}>".format(cuisine_name=self.cuisine_name, search_value=self.search_value)


class Recipe(db.Model):
    """Recipe model (user saved recipes). Units for carbs and sugar - grams (g).
        Units for calories - calories (kcal). Units for total time (seconds).
    """

    __tablename__ = "recipes"

    # recipe_id = db.Column(db.Integer,
    #                       primary_key=True,
    #                       autoincrement=True)
    yummly_recipe_id = db.Column(db.String(200), primary_key=True,
                                 autoincrement=False, nullable=False)
    name = db.Column(db.String(100), nullable=False)
    carbs = db.Column(db.Float, nullable=True)
    sugar = db.Column(db.Float, nullable=True)
    calories = db.Column(db.Integer, nullable=True)
    img_url_sm = db.Column(db.String(200), nullable=True)
    img_url_md = db.Column(db.String(200), nullable=True)
    img_url_lg = db.Column(db.String(200), nullable=True)
    total_time = db.Column(db.Integer, nullable=True)
    ingredients = db.Column(db.PickleType, nullable=True)

    def __repr__(self):
        """Show info about recipe"""

        return "<Recipe id={yummly_recipe_id} name={name} total_time={total_time} carbs={carbs} >".format(yummly_recipe_id=self.yummly_recipe_id, name=self.name, total_time=self.total_time, carbs=self.carbs)


class Allergy(db.Model):
    """Allergy model (yummly meta data; static values which restrict general
        search query parameters). Search query calculated by
        {allergy_id}^{shortDescription}
    """

    __tablename__ = "allergies"

    allergy_id = db.Column(db.String(30), primary_key=True)
    short_description = db.Column(db.String(30), nullable=False)

    def __repr__(self):
        """Show info about allergy"""

        return "<Allergy id={allergy_id} short_description={short_description}".format(allergy_id=self.allergy_id, short_description=self.short_description)


class Excluded_Ingredient(db.Model):
    """Excluded ingredients model (yummly meta data; static values which restrict)
        search query parameters).
    """

    __tablename__ = "excluded_ingredients"

    excluded_ingredient_term = db.Column(db.String(150), primary_key=True)
    search_value = db.Column(db.String(150), nullable=False)

    def __repr__(self):
        """Show info about excluded ingredient"""

        return "<ExcludedIngredient excluded_ingredient_term={excluded_ingredient_term} search_value={search_value}>".format(excluded_ingredient_term=self.excluded_ingredient_term, search_value=self.search_value)


# MIDDLE TABLE
####################

# 020618 refactoring this to use to get recipe_list data
class UserRecipe(db.Model):
    """Middle table between User and Recipe tables"""

    __tablename__ = "users_recipes"

    users_recipes_id = db.Column(db.Integer,
                                 primary_key=True,
                                 autoincrement=True)
    user_id = db.Column(db.Integer,
                        db.ForeignKey('users.user_id'))
    yummly_recipe_id = db.Column(db.String(200),
                                 db.ForeignKey('recipes.yummly_recipe_id'))
    # NOTE this is needed for grouping recipes by a user's list (mult lists)
    delivery_date = db.Column(db.DateTime, nullable=False)
    saved = db.Column(db.Boolean, nullable=False)
    user_rating = db.Column(db.Integer, nullable=True)

    user = db.relationship("User", backref="users_recipes")
    recipe = db.relationship("Recipe", backref="users_recipes")


class UserAllergy(db.Model):
    """Middle table b/t User and Allergy tables"""

    ___tablename__ = "users_allergies"

    users_allergies_id = db.Column(db.Integer,
                                   primary_key=True,
                                   autoincrement=True)
    user_id = db.Column(db.Integer,
                        db.ForeignKey('users.user_id'))
    allergy_id = db.Column(db.String(30),
                           db.ForeignKey('allergies.allergy_id'))

    # means that after having retrieved the UserAllergy class object(s) from a
    # sqlalchemy query, will be able to access related user or allergy data
    # like user_allergy_record.user.email
    user = db.relationship("User", backref="users_allergies")
    allergy = db.relationship("Allergy", backref="users_allergies")


class UserExcludedIngredient(db.Model):
    """Middle table b/t User and Excluded Ingredient tables"""

    __tablename__ = "users_excluded_ingredients"

    users_excluded_ingreds_id = db.Column(db.Integer,
                                          primary_key=True,
                                          autoincrement=True)
    user_id = db.Column(db.Integer,
                        db.ForeignKey('users.user_id'))
    excluded_ingredient_term = db.Column(db.String(30),
                                         db.ForeignKey('excluded_ingredients.excluded_ingredient_term'))

    user = db.relationship("User", backref="users_excluded_ingredients")
    excluded_ingredient = db.relationship("Excluded_Ingredient", backref="users_excluded_ingredients")


# ASSOCIATION TABLES
####################

class RecipeSettingsCuisine(db.Model):
    """Association table between Cuisine Type and Recipe Settings tables.
        Mainly expect it to be used where given a settings_recipe_id, will get
        a resulting list of cuisine types the user is interested in.
    """

    __tablename__ = "recipe_settings_cuisines"

    recipesettings_cuisine_id = db.Column(db.Integer,
                                          primary_key=True,
                                          autoincrement=True)
    cuisine_id = db.Column(db.Integer,
                           db.ForeignKey('cuisine_types.cuisine_id'),
                           nullable=False)
    recipe_settings_id = db.Column(db.Integer,
                                   db.ForeignKey('recipe_settings.recipe_settings_id'),
                                   nullable=False)

    recipe_preferences = db.relationship("Recipe_Settings",
                                         backref="recipe_settings_cuisines")
    cuisine = db.relationship("Cuisine_Type",
                              backref="recipe_settings_cuisines")


# TODO ?
# class RecipeCuisine(db.Model):


##############################################################################
# Helper functions

def init_app():
    # So that we can use Flask-SQLAlchemy, we'll make a Flask app.
    from flask import Flask
    app = Flask(__name__)

    connect_to_db(app)


def connect_to_db(app):
    """Connect the database to our Flask app."""

    # Configure to use our database.
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres:///seedsoul'
    app.config['SQLALCHEMY_ECHO'] = False
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.app = app
    db.init_app(app)
    # SELF notes probably only run this once?
    db.create_all()


if __name__ == "__main__":
    # As a convenience, if we run this module interactively, it will leave
    # you in a state of being able to work with the database directly.

    # So that we can use Flask-SQLAlchemy, we'll make a Flask app.
    from flask import Flask

    app = Flask(__name__)

    connect_to_db(app)
    print "Connected to DB."
