### Seed to Soul

Seed to Soul is an automated recipe search service.

**Target Audience**

* People who are watching their carbs (diabetic type I and type II; people who want to eat in a more healthy way). 
* People who are busy and don’t have a lot of time (motivation) to search recipes regularly but want to have variety in their meals. 
* People who mix up cooking and eating out (maybe want to cook more but are unmotivated to start).

**Features**

* Allow user to save recipe + notification preferences.
* Have the system poll the yummly api (at weekly intervals) and notify the user (SMS) when their scheduled recipe recommendations are ready. 
* Allow user to search recipes on the fly.
* Allow user to save (rate) recipes they have already viewed.
* The searches (automated and manual) will not support all the filters that yummly provides simply because they too voluminous and at a certain point, being too flexible requires more time from the user.

Screenshot of recipes


![Alt text](https://github.com/cch5ng/seed-to-soul/blob/158_screenshots_readme/screenshots/screen_recipe_list.png?raw=true "Optional Title")


Screenshot of settings


![Alt text](https://github.com/cch5ng/seed-to-soul/blob/158_screenshots_readme/screenshots/screen_settings.png?raw=true "Optional Title")


### Dependencies

* PostgreSQL database 10.1
* Python 2.7
* Node 7.10.1 (using nvm)
* React v16+
* React Router v4+
* Yummly API (requires app_id and key)
* Twilio API ()

#### Secrets file

At the project root, create a file to store API keys.

`touch secrets.sh`

The contents should look like:

```
export YUMMLY_APP_ID="<replace_with_yummly_app_id>"
export YUMMLY_APP_KEY="<replace_with_yummly_app_key>"
export SENDGRID_API_KEY="<replace_with_sendgrid_api_key">
# jwt_key is the secret
export JWT_KEY="<replace_with_your_jwt_key>"
```


### To install the app

#### Server

For this iteration, pipenv was used (in place of pip and virtualenv). pipenv was installed using homebrew.

`brew install pipenv`


Specify python version.


`pipenv --python 2.7.10`


Install packages.


`pipenv install`


Start virtual environment.


`pipenv shell`


Database setup follows and could be done in a separate terminal tab.

Start the PostgresSQL database. Then create a database.


`createdb seedsoul`


Create the schema.


`python model.py`


Access secrets.


`source secrets.sh`


Populate the database.


`python seed.py`



#### Client

From client directory, install files. This should be done in a separate terminal tab.

It might be necessary to install an older node version 7.10.1 using nvm, prior to installing packages.


```
cd static
yarn install \\ or npm install
```


### To run the app

Note that the current setup is using a workaround. The Flask server is being used to serve the bundled JS package.

This should change for a proper dev and production environment but was a workaround used for the time restrictions.

In production and a dev environment (using webpack without the use of create-react-app for scaffolding), an Express router should be added and either direct requests on port 8080 to the React front-end or proxy api requests to the Flask routes served on port 5000.


#### Client

Build the JS package (from /static directory).

`npm run build`


#### Server

Start Flask. This should be done from the pipenv virtual environment.

`python server.py`


Afterward the app should be running at http://localhost:5000

It is necessary to register a new user in order to log in and update recipe preferences (settings).


#### Scheduler

A cron job should be configured in order to make the periodic recipe queries to the recipe API. This is the format of the cron job:

```
MAILTO=<user_name>
SHELL=/bin/bash
00 10 * * * (/bin/date;source /<project_path>/secrets.sh && /<path_to_virtualenv>/bin/python /<project_path>/cron_recipes.py) 2>&1 >> /<project_path>/log.txt

```

* Currently it is hard-coded to query the recipe API once a day at 10am for those users who set their notification to the current day.

* Using the MAILTO configuration can be useful for troubleshooting, to view errors with the cron job.

* A common issue is pointing to the wrong python binary, when using a virtual environment, the binary specific to the virtual environment needs to be used explicitly.

* A log file should be available with date/time stamps for each time the cron job is run.


For more info: https://www.adminschoice.com/crontab-quick-reference


### Credits

The boilerplate for this project was built upon:
* a couple existing projects
  * https://github.com/dternyak/React-Redux-Flask
    * this project does not include as many of the npm modules but uses his pattern of using express between react and flask routes
  * https://expressjs.com/en/starter/generator.html
    * the express web server code (current `/static/bin/www`) is largely coming from this project
  * https://github.com/facebook/create-react-app
    * the react boilerplate was from create-react-app
* an article by Dave Ceddia (https://daveceddia.com/create-react-app-express-backend/)
