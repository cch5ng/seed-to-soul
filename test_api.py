import unittest
import mock
from server import (yummly_get_request, get_yummly_recipe_detail, app,
                    get_user_id)
import requests
import requests_mock
from model import (db, User, Recipe_Settings, Recipe,
                   UserRecipe, Cuisine_Type, RecipeSettingsCuisine,
                   UserAllergy, Excluded_Ingredient, UserExcludedIngredient,
                   UserAllergy, Allergy)
from flask import Flask, redirect, request, render_template, session, jsonify
from seed import (load_test_user, load_test_recipes, load_allergies,
                  load_ingredients, load_test_cuisines,
                  load_test_recipe_settings)

# QUEUE could not get patch mock working
class TestAPIMocks(unittest.TestCase):
    # QUEUE believe this is issue
    # do I need to define a class in order to identify which get request to mock?

    def _mock_response(
            self,
            status=200,
            content="CONTENT",
            json_data=None,
            raise_for_status=None):
        """
        helper function for mock http responses
        from https://gist.github.com/evansde77/45467f5a7af84d2a2d34f3fcb357449c
        """
        mock_resp = mock.Mock()
        # mock raise_for_status call w/optional error
        # mock_resp.raise_for_status = mock.Mock()
        # if raise_for_status:
        #    mock_resp.raise_for_status.side_effect = raise_for_status
        # set status code and content
        mock_resp.status_code = status
        mock_resp.content = content
        # add json data if provided
        if json_data:
            mock_resp.json = mock.Mock(
                return_value=json_data
            )
            print 'mock_resp.json', mock_resp.json
        return mock_resp

    @mock.patch('requests.get')  # Mock 'requests' module 'get' method.
    def test_yummly_recipe_summary_request_success(self, mock_get):
        """Mocking successful yummly api response (recipes summary)."""

        mock_resp = self._mock_response(content="elephants")
        mock_get.return_value = mock_resp
        url = "http://api.yummly.com/v1/api/recipes"
        params = {'allowedAllergy[]': ['400^Soy-Free', '396^Dairy-Free',
                  '397^Egg-Free'], 'requirePictures': True,
                  'allowedCuisine[]': ['cuisine^cuisine-italian'],
                  'excludedIngredient[]': [u'chicken']}

        response = yummly_get_request(url, params)
        print 'response', response

        self.assertEqual(response, 'elephants')

    @mock.patch('requests.get')
    def test_yummly_recipe_summary_request_error(self, mock_get):
        """Mocking failed yummly api response (http 500 error)."""

        mock_resp = self._mock_response(
                    status=500,
                    content='{"status_code": 500, "error": "recipes could not be retrieved"}')
        mock_get.return_value = mock_resp
        url = "http://api.yummly.com/v1/api/recipes"
        params = {'allowedAllergy[]': ['400^Soy-Free', '396^Dairy-Free',
                  '397^Egg-Free'], 'requirePictures': True,
                  'allowedCuisine[]': ['cuisine^cuisine-italian'],
                  'excludedIngredient[]': [u'chicken']}

        response = yummly_get_request(url, params)
        print 'response', response

        self.assertEqual(response, '{"status_code": 500, "error": "recipes could not be retrieved"}')

    # TOFIX: couldn't figure out syntax when function returns json obj
    # @mock.patch('requests.get')  # Mock 'requests' module 'get' method.
    # def test_yummly_recipe_detail_request_success(self, mock_get):
    #     """Mocking successful yummly api response (recipes summary)."""

    #     mock_resp = self._mock_response(json_data={"status_code": 200, "recipe": "mock recipe"})
    #     mock_get.return_value = mock_resp
    #     url = "http://api.yummly.com/v1/api/recipes"
    #     params = {'allowedAllergy[]': ['400^Soy-Free', '396^Dairy-Free',
    #               '397^Egg-Free'], 'requirePictures': True,
    #               'allowedCuisine[]': ['cuisine^cuisine-italian'],
    #               'excludedIngredient[]': [u'chicken']}

    #     response = get_yummly_recipe_detail('recipe_id')
    #     print 'response', response

    #     self.assertEqual(response.json, {"status_code": 200, "recipe": "mock recipe"})

    # TOFIX: couldn't figure out syntax when function returns json obj
    # @mock.patch('requests.get')
    # def test_yummly_recipe_detail_request_error(self, mock_get):
    #     """Mocking failed yummly api response (http 500 error)."""

    #     mock_resp = self._mock_response(
    #                 status=500,
    #                 content='{"status_code": 500, "error": "recipes could not be retrieved"}')
    #     mock_get.return_value = mock_resp
    #     url = "http://api.yummly.com/v1/api/recipes"
    #     params = {'allowedAllergy[]': ['400^Soy-Free', '396^Dairy-Free',
    #               '397^Egg-Free'], 'requirePictures': True,
    #               'allowedCuisine[]': ['cuisine^cuisine-italian'],
    #               'excludedIngredient[]': [u'chicken']}

    #     response = yummly_get_request(url, params)
    #     print 'response', response

    #     self.assertEqual(response, '{"status_code": 500, "error": "recipes could not be retrieved"}')

class TestDBMocks(unittest.TestCase):
    """Defines a set of test mocking queries to the database.
    """

    def setUp(self):
        """Done before each test.
        """

        self.app = Flask(__name__)
        self.app.config['TESTING'] = True

        connect_to_db(self.app, "postgresql:///testdb")

        # load seed data (should be one function call?)
        with self.app.app_context():
            load_test_user()
            load_test_recipes()
            load_allergies()
            load_ingredients()
            load_test_cuisines()
            load_test_recipe_settings()

    def tearDown(self):
        """
        """

        with self.app.app_context():
           db.session.close()
           db.drop_all()

    def test_get_user_id_success(self):
        """Verifies that given a valid user email, a user id should be
            returned.
        """

        with self.app.app_context():
            id = get_user_id('c@c.com')
            self.assertEqual(id, 9999, 'given email should output user id')


    def test_get_user_id_err(self):
        """Verifies that given an invalid user email, None value should be
            returned.
        """

        with self.app.app_context():
            id = get_user_id('z@z.com')
            self.assertEqual(id, None)


# HELPER


def connect_to_db(app, uri):
    """Connect the database to our Flask app."""

    # Configure to use our database.
    app.config['SQLALCHEMY_DATABASE_URI'] = uri
    app.config['SQLALCHEMY_ECHO'] = False
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    #db.app = app
    db.init_app(app)
    with app.app_context():
            db.create_all()
    print 'gets to connect_to_db local'


if __name__ == '__main__':
    unittest.main()
