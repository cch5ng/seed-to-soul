import bcrypt
# import datetime
from datetime import date, timedelta, datetime as dt
from flask import Flask, redirect, request, render_template, session, jsonify
from flask_debugtoolbar import DebugToolbarExtension
from jinja2 import StrictUndefined
import json
import os
import pickle
import requests
from twilio.rest import Client
import uuid
import jwt
from model import (connect_to_db, db, User, Recipe_Settings, Recipe,
                   UserRecipe, Cuisine_Type, RecipeSettingsCuisine,
                   UserAllergy, Excluded_Ingredient, UserExcludedIngredient)


# modified static folder and template folder to run with react app
app = Flask(__name__, static_folder="static/build/static",
            template_folder="static/build")
app.jinja_env.undefined = StrictUndefined
app.jinja_env.auto_reload = True

# Required to use Flask sessions and the debug toolbar
app.secret_key = "ABC"
jwt_key = os.environ['JWT_KEY']

# yummly api-endpoints
URL = "http://api.yummly.com/v1/api/recipes"
consumer_id = os.environ['YUMMLY_APP_ID']
consumer_key = os.environ['YUMMLY_APP_KEY']

_ALLERGIES_DESC_TO_ID = {
    'Gluten-Free': '393',
    'Peanut-Free': '394',
    'Seafood-Free': '398',
    "Sesame-Free": '399',
    'Soy-Free': '400',
    'Dairy-Free': '396',
    'Egg-Free': '397',
    'Sulfite-Free': '401',
    'Tree Nut-Free': '395',
    'Wheat-Free': '392'
}

YUMMLY_ID_TO_SHORT_DESC = {"388": "Lacto vegetarian",
                           "389": "Ovo vegetarian",
                           "390": "Pescetarian",
                           "386": "Vegan",
                           "387": "Lacto-ovo vegetarian",
                           "403": "Paleo"}


# test data
test_nutrient_list = [{"attribute": "SUGAR",
                       "value": 12.13,
                       },
                      {"attribute": "CHOCDF",
                       "value": 25.33,
                       },
                      {"attribute": "ENERC_KCAL",
                       "value": 150}]

test_nutrient_no_cal_carb = [{"attribute": "SUGAR",
                              "value": 12.13}]

test_nutrient_no_sugar = [{"attribute": "CHOCDF",
                           "value": 25.33},
                          {"attribute": "ENERC_KCAL",
                           "value": 150}]

# ROUTES


@app.route('/', methods=['GET'])
def index():
    """Route from https://github.com/dternyak/React-Redux-Flask
    """
    return render_template('index.html')


@app.route('/<path:path>', methods=['GET'])
def any_root_path(path):
    """Route from https://github.com/dternyak/React-Redux-Flask. Required to
        support client side routing (react router).
    """
    return render_template('index.html')


@app.route('/api/recipe_settings', methods=['POST'])
def get_recipe_settings():
    """Given a user_id, retrieve the recipe_settings table
        with user's current search preferences. Generally every registered user
        should have a recipe settings record.
    """

    # Chang suggestion on how to refactor code
    # rec_settings = {'maxCarbs': 0, 'notifyDay': 'Saturday'}
    # rec_settings['maxCarbs'] = get_max_carb_setting(user_id)
    # rec_settings['notification_day'] = get_max_carb_setting(user_id)
    # rec_settings['cuisines'] = get_max_carb_setting(user_id)
    # rec_settings['allergies'] = get_max_carb_setting(user_id)
    # rec_settings['exludeIngredients'] = get_max_carb_setting(user_id)
    # return jsonify(rec_settings)

    # default settings if none exists for user
    rec_settings = {'maxCarbs': 0, 'maxSugar': 0, 'notifyDay': 'Saturday'}

    access_token = request.headers['Authorization'].split(' ')[1]
    email = get_email_from_access_token(access_token, jwt_key)

    user_rec = db.session.query(User).filter(User.email == email).first()
    recipe_setting_rec = db.session.query(Recipe_Settings).filter(Recipe_Settings.user_id == user_rec.user_id).first()

    if recipe_setting_rec is None:
        return jsonify(rec_settings)
    else:
        if recipe_setting_rec.max_carbs is not None:
            rec_settings['maxCarbs'] = recipe_setting_rec.max_carbs
        if recipe_setting_rec.max_sugar is not None:
            rec_settings['maxSugar'] = recipe_setting_rec.max_sugar
        if recipe_setting_rec.notification_day is not None:
            rec_settings['notifyDay'] = recipe_setting_rec.notification_day

        recipe_settings_cuisine_recs = db.session.query(RecipeSettingsCuisine).filter(RecipeSettingsCuisine.recipe_settings_id == recipe_setting_rec.recipe_settings_id).all()
        if recipe_settings_cuisine_recs is not None:
            cuisines = []
            for rs in recipe_settings_cuisine_recs:
                cuisines.append(rs.cuisine.search_value)
        rec_settings['cuisines'] = cuisines

        user_allergy_recs = db.session.query(UserAllergy).filter(UserAllergy.user_id == user_rec.user_id).all()
        if user_allergy_recs is not None:
            allergies = []
            for ar in user_allergy_recs:
                allergies.append(ar.allergy.short_description)
        rec_settings['allergies'] = allergies

        # REFACTOR list comprehension
        user_exclude_ingred_recs = db.session.query(UserExcludedIngredient).filter(UserExcludedIngredient.user_id == user_rec.user_id).all()
        if user_exclude_ingred_recs is not None:
            excluded_ingredients = []
            for ing in user_exclude_ingred_recs:
                excluded_ingredients.append(ing.excluded_ingredient.search_value)
        rec_settings['excludedIngredients'] = excluded_ingredients

        return jsonify(rec_settings)


@app.route('/api/update_recipe_settings', methods=['POST'])
def update_recipe_settings():
    """Given a user_id and settingsObject, update the recipe_settings table
        with user's current search preferences.
    """

    req_data = request.get_json()
    access_token = request.headers['Authorization'].split(' ')[1]
    email = get_email_from_access_token(access_token, jwt_key)
    user_id = get_user_id(email)

    if user_id is None:
        return '{"result": "error", "message": "User not found. Please log in again."}'

    # REFACTOR, probably iterate over keys?
    else:
        if 'maxCarbs' in req_data:
            max_carbs = req_data['maxCarbs']
        else:
            max_carbs = None
        if 'maxSugar' in req_data:
            max_sugar = req_data['maxSugar']
        else:
            max_sugar = None
        if 'cuisine' in req_data:
            ethnic_cuisine = req_data['cuisine']
        else:
            ethnic_cuisine = None
        if 'notifyDay' in req_data:
            notification_day = req_data['notifyDay']
        else:
            notification_day = None
        if 'allergiesList' in req_data:
            allergies_list = req_data['allergiesList']
        else:
            allergies_list = None
        if 'excludedIngredientsList' in req_data:
            excluded_ingredients = req_data['excludedIngredientsList']
        else:
            excluded_ingredients = None

        cur_setting = db.session.query(Recipe_Settings).filter(Recipe_Settings.user_id == user_id).first()
        if cur_setting is not None:
            recipe_setting_rec = db.session.query(Recipe_Settings).filter(Recipe_Settings.user_id == user_id).first()
            rs_cuisines = db.session.query(RecipeSettingsCuisine).filter(RecipeSettingsCuisine.recipe_settings_id == recipe_setting_rec.recipe_settings_id).all()
            if max_carbs is not None:
                recipe_setting_rec.max_carbs = max_carbs
            if max_sugar is not None:
                recipe_setting_rec.max_sugar = max_sugar
            if notification_day is not None:
                recipe_setting_rec.notification_day = notification_day

            for rsc in rs_cuisines:
                if rsc.cuisine.cuisine_name not in ethnic_cuisine:
                    db.session.delete(rsc)
                    db.session.commit()
        else:
            recipe_setting_rec = Recipe_Settings(max_carbs=max_carbs,
                                                 max_sugar=max_sugar,
                                                 notification_day=notification_day,
                                                 user_id=user_id)
            db.session.add(recipe_setting_rec)
            db.session.commit()

        # TODO create a helper
        rs_cuisine_names = []
        # TODO check if this needs a condition related to line 162?
        for r in rs_cuisines:
            rs_cuisine_names.append(r.cuisine.cuisine_name)

        for c in ethnic_cuisine:
            if c not in rs_cuisine_names:
                cur_cuisine = db.session.query(Cuisine_Type).filter(Cuisine_Type.search_value == c).first()
                new_recipe_settings_cuisine = RecipeSettingsCuisine(cuisine_id=cur_cuisine.cuisine_id, recipe_settings_id=recipe_setting_rec.recipe_settings_id)
                db.session.add(new_recipe_settings_cuisine)

        # TODO create a helper
        db_user_allergy_recs = db.session.query(UserAllergy).filter(UserAllergy.user_id == user_id).all()
        db_user_allergy_names = []
        # create a list of saved allergies for given user
        for r in db_user_allergy_recs:
            db_user_allergy_names.append(r.allergy.short_description)
        # for newly saved allergies, insert rec in db
        for a in allergies_list:
            if a not in db_user_allergy_names:
                aid = _ALLERGIES_DESC_TO_ID[a]
                new_a = UserAllergy(user_id=user_id, allergy_id=aid)
                db.session.add(new_a)
        # for newly removed allergies (rec settings), del rec from db
        for b in db_user_allergy_names:
            if b not in allergies_list:
                if b in _ALLERGIES_DESC_TO_ID:
                    aid = _ALLERGIES_DESC_TO_ID[b]
                    old_userallergy_rec = db.session.query(UserAllergy).filter(UserAllergy.allergy_id == aid ).first()
                    db.session.delete(old_userallergy_rec)
                    db.session.commit()

        # TODO helper (try to refactor db logic for cuisine, allergy, exc ingredients into an abstract function; passing in table, etc)
        db_user_excluded_ingredient_recs = db.session.query(UserExcludedIngredient).filter(UserExcludedIngredient.user_id == user_id).all()
        db_user_excluded_ingredients = []
        for ing in db_user_excluded_ingredient_recs:
            db_user_excluded_ingredients.append(ing.excluded_ingredient.search_value)
        # add newly saved excluded ingredient to db
        for c in excluded_ingredients:
            if c not in db_user_excluded_ingredients:
                new_c = UserExcludedIngredient(user_id=user_id, excluded_ingredient_term=c)
                db.session.add(new_c)
        # del newly removed excluded ingredient from db
        for d in db_user_excluded_ingredients:
            if d not in excluded_ingredients:
                old_user_exclude_ingred = db.session.query(UserExcludedIngredient).filter(UserExcludedIngredient.excluded_ingredient_term == d).first()
                db.session.delete(old_user_exclude_ingred)
                db.session.commit()

        db.session.commit()

        # REFACTOR optional whether to return a status or just return None; return
        return '{"result": "success"}'


@app.route('/api/recipe_search/ingredients.json')
def get_ingredients_to_exclude():
    """Route to get a json file for ingredients to exclude from recipes search.
        Resulting data used by FE to provide auto-suggest feature on the
        recipe settings form. Yummly API restricts search query to this list.
        (tested with limit(10000))
    """

    ingredients_recs = db.session.query(Excluded_Ingredient.search_value).all()
    print 'ingredients_recs', ingredients_recs
    ingredients = []
    for r in ingredients_recs:
        ingredients_dict = {}
        ingredients_dict['value'] = r[0]
        ingredients_dict['label'] = r[0]
        ingredients.append(ingredients_dict)
    return jsonify(ingredients)


@app.route('/api/recipe_search', methods=['POST'])
def search_recipe_list(email=None):
    """Generally called from a cron script to run automated searches by user.
        Currently also triggered from a test button on FE \test route. Makes a
        GET request to yummly for recipe summary search. When this method
        called by cron script, email will be provided. Otherwise for internal
        testing, email will be None and provided in payload.
    """

    # case internal testing from /test route
    if email is None:
        req_data = request.get_json()
        email = req_data['email']

    # REFACTOR this section, to get user rec is used frequently
    user_rec = db.session.query(User).filter(User.email == email).first()
    rec_settings_rec = db.session.query(Recipe_Settings).filter(Recipe_Settings.user_id == user_rec.user_id).first()

    max_carbs_settings = rec_settings_rec.max_carbs
    max_sugar_settings = rec_settings_rec.max_sugar

    db_rs_cuisines = db.session.query(RecipeSettingsCuisine).filter(RecipeSettingsCuisine.recipe_settings_id == rec_settings_rec.recipe_settings_id).all()
    db_user_allergy_rec = db.session.query(UserAllergy).filter(UserRecipe.user_id == user_rec.user_id).all()
    db_user_excluded_ingred_rec = db.session.query(UserExcludedIngredient).filter(UserExcludedIngredient.user_id == user_rec.user_id).all()

    # by default only get recipes that include pictures
    yummly_params = {}
    max_carbs_params = {}
    cuisine_params = {}
    allergy_params = {}
    excluded_ingredients_params = {}
    require_pictures = {'requirePictures': True}

    cuisine_names = []
    for c in db_rs_cuisines:
        cuisine_names.append(c.cuisine.search_value)
    formatted_cuisine_list = get_yummly_cuisine_query_str(cuisine_names)

    allergies = []
    for a_rec in db_user_allergy_rec:
        rec_dict = {a_rec.allergy.allergy_id: a_rec.allergy.short_description}
        allergies.append(rec_dict)

    excluded_ingredients = []
    for b_rec in db_user_excluded_ingred_rec:
        excluded_ingredients.append(b_rec.excluded_ingredient.search_value)
    if len(excluded_ingredients) > 0:
        excluded_ingredients_params = {'excludedIngredient[]': excluded_ingredients}
        yummly_params.update(excluded_ingredients_params)

    if len(cuisine_names) > 0:
        cuisine_params = {'allowedCuisine[]': formatted_cuisine_list}
        yummly_params.update(cuisine_params)
    if max_carbs_settings != 0:
        max_carbs_params = {'nutrition.CHOCDF.min': 0,
                            'nutrition.CHOCDF.max': max_carbs_settings}
        yummly_params.update(max_carbs_params)
    if max_sugar_settings != 0:
        max_sugar_params = {'nutrition.SUGAR.min': 0,
                            'nutrition.SUGAR.max': max_sugar_settings}
        yummly_params.update(max_sugar_params)

    if len(allergies) > 0:
        list_allergy_values = get_yummly_allergy_query_str(allergies)
        allergy_params = {'allowedAllergy[]': list_allergy_values}
        yummly_params.update(allergy_params)

    yummly_params.update(require_pictures)

    headers = {'X-Yummly-App-ID': consumer_id,
               'X-Yummly-App-Key': consumer_key}
    r = requests.get(url=URL, params=yummly_params, headers=headers, timeout=5)
    if r.status_code == 200:
        yummly_summary_result = r.json()

        # TODO make async
        populate_recipe_table(yummly_summary_result, user_rec.user_id)
        get_nutrients_from_recipe_summary(yummly_summary_result['matches'])

        return r.text
    else:
        return_status = r.status_code
        return "{'error': '3rd party api returned an error', 'status': ${return_status}}".format(return_status=return_status)


@app.route('/api/recipes', methods=['POST'])
def get_current_recipe_list():
    """Given email, returns current recommended recipes list.
        Try on 022618 to return all recipes associated with that user. Sorted
        by delivery date and alphabetically on name. Using post to get email
        from FE.
    """

    access_token = request.headers['Authorization'].split(' ')[1]
    email = get_email_from_access_token(access_token, jwt_key)

    user = db.session.query(User).filter(User.email == email).first()
    user_recipes = db.session.query(UserRecipe).filter(UserRecipe.user_id == user.user_id).order_by(UserRecipe.users_recipes_id.desc()).all()
    latest_delivery_date = user_recipes[0].delivery_date

    cur_user_recipes = []
    for r in user_recipes:
        recipe_dict = {}
        recipe_dict['yummly_recipe_id'] = r.recipe.yummly_recipe_id
        recipe_dict['name'] = r.recipe.name
        recipe_dict['total_time'] = r.recipe.total_time
        recipe_dict['img_url_sm'] = r.recipe.img_url_sm
        recipe_dict['ingredients'] = r.recipe.ingredients
        recipe_dict['saved'] = r.saved
        recipe_dict['user_rating'] = r.user_rating
        recipe_dict['delivery_date'] = r.delivery_date

        # check to prevent app error when no data associated
        if r.recipe.calories is not None:
            recipe_dict['calories'] = r.recipe.calories
        if r.recipe.carbs is not None:
            recipe_dict['carbs'] = r.recipe.carbs
        if r.recipe.sugar is not None:
            recipe_dict['sugar'] = r.recipe.sugar
        if r.recipe.img_url_md is not None:
            recipe_dict['img_url_md'] = r.recipe.img_url_md
        if r.recipe.img_url_lg is not None:
            recipe_dict['img_url_lg'] = r.recipe.img_url_lg

        cur_user_recipes.append(recipe_dict)

    result_json = jsonify(cur_user_recipes)
    return result_json


@app.route('/api/recipes/update', methods=['POST'])
def updateRecipeStatus():
    """Given recipe_id and user email, provides recipe status to update in db,
        UserRecipe table. Fields include saved, user_rating.
    """

    req_data = request.get_json()
    recipe_id = req_data['recipeId']
    email = req_data['email']
    user_rec = db.session.query(User).filter(User.email == email).first()
    user_recipe_rec = db.session.query(UserRecipe).filter(UserRecipe.user_id == user_rec.user_id, UserRecipe.yummly_recipe_id == recipe_id).first()

    # toggle the current state for the button
    if 'saved' in req_data:
        saved = not req_data['saved']
        user_recipe_rec.saved = saved
        result = {"recipe_id": recipe_id, "saved": saved}
    if 'userRating' in req_data:
        user_rating = req_data['userRating']
        user_recipe_rec.user_rating = user_rating
        result = {"recipe_id": recipe_id, "user_rating": user_rating}

    db.session.commit()
    return jsonify(result)


@app.route('/api/recipe_quick_search', methods=['POST'])
def get_dynamic_search():
    """Makes a GET request to yummly api for recipes list (summary) given a
        user's real-time search request.
        Currently not storing dynamic search results to database. Future enh?
    """

    req_data = request.get_json()
    print 'req_data', req_data
    minNutrientAttrs = ['TOCPHA', 'VITA_RAE', 'VITC', 'VITK']
    yummly_params = {}
    # require_pictures = {'requirePictures': True}
    # yummly_params.update(require_pictures)

    if 'recipeName' in req_data and len(req_data['recipeName']):
        yummly_params.update({'q': req_data['recipeName']})

    if req_data['maxCarbs'] > 0:
        max_carbs_params = {'nutrition.CHOCDF.min': 0,
                            'nutrition.CHOCDF.max': req_data['maxCarbs']}
        yummly_params.update(max_carbs_params)

    for n in minNutrientAttrs:
        if req_data[n] > 0:
            vitMin = req_data[n]
            vitKey = get_yummly_nutrient_query_str(n)
            vitDict = {}
            vitDict[vitKey] = vitMin
            yummly_params.update(vitDict)

    if 'dietsList' in req_data and len(req_data['dietsList']) > 0:
        diets_params = []
        for d in req_data['dietsList']:
            diets_params.append(get_yummly_diet_query_str(d))
        yummly_params.update({'allowedDiet[]': diets_params})

    if 'cuisine' in req_data:
        formatted_cuisine_list = get_yummly_cuisine_query_str(req_data['cuisine'])
        cuisine_params = {'allowedCuisine[]': formatted_cuisine_list}
        yummly_params.update(cuisine_params)

    if 'excludedIngredientsList' in req_data:
        excluded_ingredients_params = {'excludedIngredient[]': req_data['excludedIngredientsList']}
        yummly_params.update(excluded_ingredients_params)

    if 'allergiesList' in req_data:
        # req_data['allergiesList'] is a list of search_values but to make the
        # query also need to retrieve the allergy_id
        allergies_dicts = []
        for a in req_data['allergiesList']:
            rec_dict = {_ALLERGIES_DESC_TO_ID[a]: a}
            allergies_dicts.append(rec_dict)

        list_allergy_values = get_yummly_allergy_query_str(allergies_dicts)
        allergy_params = {'allowedAllergy[]': list_allergy_values}
        yummly_params.update(allergy_params)

    json_resp = yummly_get_request(URL, yummly_params)

    return json_resp


@app.route('/api/registration', methods=['POST'])
def register_user():
    """Given a user's registration info (email, password, phone), add user data
        to the User table. Returns an auth token (for demo this is a fake
        token). TODO use JWT in future.
    """

    req_data = request.get_json()
    email = req_data['email']
    password = req_data['password']
    hashed = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt())

    new_user = User(email=email, password=hashed,
                    phone=req_data['phone'])
    db.session.add(new_user)
    db.session.commit()
    user_rec = db.session.query(User).filter(User.email == email).first()
    new_settings = Recipe_Settings(max_carbs=0, max_sugar=0,
                                   notification_day='Saturday',
                                   user_id=user_rec.user_id)
    db.session.add(new_settings)

    delivery_date = date.today()
    # prepopulate a few recipes so initial homepage view is not empty

    recipe1 = UserRecipe(user_id=user_rec.user_id, yummly_recipe_id='Mediterranean-Turkey-Pinwheels-1345729',
                         delivery_date=delivery_date, saved=False,
                         user_rating=0)
    recipe2 = UserRecipe(user_id=user_rec.user_id,
                         yummly_recipe_id='Ranch-Pork-Chops-and-Potatoes-Sheet-Pan-Dinner-2237978',
                         delivery_date=delivery_date, saved=False,
                         user_rating=0)
    recipe3 = UserRecipe(user_id=user_rec.user_id,
                         yummly_recipe_id='Pizza-1106814',
                         delivery_date=delivery_date, saved=False,
                         user_rating=0)
    recipe4 = UserRecipe(user_id=user_rec.user_id, yummly_recipe_id='Easy-Japanese-Pickled-Cucumber-990154',
                         delivery_date=delivery_date, saved=False,
                         user_rating=0)
    recipe5 = UserRecipe(user_id=user_rec.user_id, yummly_recipe_id='Japanese-Mochi-Ice-Cream-2353310',
                         delivery_date=delivery_date, saved=False, user_rating=0)
    recipe6 = UserRecipe(user_id=user_rec.user_id, yummly_recipe_id='Lemon-Pepper-Seared-Tuna-Sashimi-575073',
                         delivery_date=delivery_date, saved=False,
                         user_rating=0)

    db.session.add_all([recipe1, recipe2, recipe3, recipe4, recipe5, recipe6])
    db.session.commit()

    # TODO generate kosher auth token
    auth_token = {"email": email, "authenticated": True, "expiration": "date"}
    return jsonify(auth_token)


@app.route('/api/login', methods=['POST'])
def login_user():
    """Given a user's login info (email, password), verify user exists with
        valid password. Returns an auth token (for demo this is a fake
        token). TODO use JWT in future.
    """

    req_data = request.get_json()
    email = req_data['email']
    print 'email from login', email
    password = req_data['password']

    user_rec = db.session.query(User).filter(User.email == email).first()
    if user_rec is not None:
        hashed = user_rec.password
        if bcrypt.checkpw(password.encode('utf8'), hashed.encode('utf8')):
            print 'user login is valid'
            token = encode_auth_token(email)
            # print 'token', token

            decoded = jwt.decode(token, jwt_key, algorithms='HS256')
            resp = {"token": token.decode(), "isAuthenticated": True}
            # auth_token = {"email": email, "authenticated": True, "expiration": "date"}
            return jsonify(resp)
        else:
            print 'user login is invalid'
            err_resp = {"status": "error", "isAuthenticated": False,
                        "message": "login was not successful, please check spelling"}
            return jsonify(err_resp)
    else: # if not user_rec
        print 'user login is invalid'
        err_resp = {"status": "error", "isAuthenticated": False,
                    "message": "login was not successful, please check spelling"}
        return jsonify(err_resp)


def encode_auth_token(email):
    """
    Generates the Auth Token
    :return: string
    https://realpython.com/blog/python/token-based-authentication-with-flask/
    """
    try:
        payload = {
            'exp': dt.utcnow() + timedelta(days=30, seconds=5),
            'iat': dt.utcnow(),
            'sub': email
        }
        return jwt.encode(
            payload,
            jwt_key,
            algorithm='HS256'
        )
    except Exception as e:
        return e


def get_email_from_access_token(access_token, jwt_key):
    """
        Given JWT token and key, returns cur user email.
    """

    # access_token = request.headers['Authorization'].split(' ')[1]
    decoded = jwt.decode(access_token, jwt_key, algorithms='HS256')
    email = decoded['sub']
    return email


def get_yummly_cuisine_query_str(cuisineList):
    """Given a list of cuisine names, returns a list of formatted strings to
        pass with get params for Yummly search request (list of recipes)

    >>> get_yummly_cuisine_query_str(['burmese', 'korean'])
    ['cuisine^cuisine-burmese', 'cuisine^cuisine-korean']
    """

    CUISINE_PREFIX = 'cuisine^cuisine-'
    resultList = []

    for c in cuisineList:
        resultList.append('{prefix}{c}'.format(prefix=CUISINE_PREFIX, c=c))
    return resultList


def get_yummly_allergy_query_str(allergyList):
    """Given a list of dictionaries (allergy_id: allergy_search_value, returns
        a list of formatted strings to pass with get params for Yummly search
        request (list of allergies)
        REFACTOR?
    """

    resultList = []

    for c in allergyList:
        for allergy_id in c.keys():
            allergy_short_desc = c[allergy_id]
            resultList.append('{allergy_id}^{allergy_short_desc}'.format(allergy_id=allergy_id, allergy_short_desc=allergy_short_desc))
    return resultList


def get_yummly_nutrient_query_str(nutrient_attr):
    """Given nutrient_attr (string), returns a string to use with the main
        yummly GET params
        TODO figure out whether to hardcode the max values
        like if user adds a min then just hard code max value as USDA recommended max?
    """

    return 'nutrition.{nutrient_attr}.min'.format(nutrient_attr=nutrient_attr)


def get_yummly_diet_query_str(diet_id):
    """Given diet_id (string), returns a string to use with the main
        yummly GET params
    """

    search_value = YUMMLY_ID_TO_SHORT_DESC[diet_id]
    return '{diet_id}^{search_value}'.format(diet_id=diet_id, search_value=search_value)


def populate_recipe_table(recipe_dict, uid):
    """Given a dictionary coming from yummly recipe search (summary list),
        extracts data and populates Recipe table, UserRecipe table. This data
        is saved to track when a particular recipe was recommended to user
        (weekly) notifications.
    """

    for r in recipe_dict['matches']:
        yummly_recipe_id = r['id']
        recipe_rec_exists = Recipe.query.get(yummly_recipe_id)
        # only insert recipe if unique; TODO need logic to handle getting more
        # when recipe(s) are dupes
        if recipe_rec_exists is None:
            name = r['recipeName']
            total_time = r['totalTimeInSeconds']
            if 'smallImageUrls' in r:
                img_url_sm = r['smallImageUrls'][0]
            else:
                img_url_sm = None
            ingredients = r['ingredients']

            recipe_rec = Recipe(yummly_recipe_id=yummly_recipe_id, name=name,
                                total_time=total_time,
                                img_url_sm=img_url_sm,
                                ingredients=ingredients)
            db.session.add(recipe_rec)
        # TODO doublecheck logic, if get recipes on same day as notify_day
        # this should work; but if get recipes earlier, this should change
        delivery_date = date.today()
        user_recipe_rec = UserRecipe(user_id=uid,
                                     yummly_recipe_id=yummly_recipe_id,
                                     delivery_date=delivery_date,
                                     saved=False,
                                     user_rating=0)
        user_recipe_rec_exists = db.session.query(UserRecipe).filter(UserRecipe.user_id==uid, UserRecipe.yummly_recipe_id==yummly_recipe_id).first()
        if user_recipe_rec_exists is None:
            db.session.add(user_recipe_rec)

    db.session.commit()


def update_recipe_detail(recipe_details):
    """Given a dictionary with recipe detail fields (id, carbs, sugar, image
        urls), updates the recipes table. Reason for saving this data is to
        reduce the number of clicks user must make to view a recipe's sugar or
        carbs level.
    """

    # Try to be more consistent with field references
    yummly_recipe_id = recipe_details['recipe_id']

    recipe_rec_exists = Recipe.query.get(yummly_recipe_id)
    if recipe_rec_exists is None:
        pass
    else:
        # checking if API returned the data, for error handling
        if 'sugar' in recipe_details:
            recipe_rec_exists.sugar = recipe_details['sugar']
        if 'carbs' in recipe_details:
            recipe_rec_exists.carbs = recipe_details['carbs']
        if 'calories' in recipe_details:
            recipe_rec_exists.calories = recipe_details['calories']
        if 'img_url_md' in recipe_details:
            recipe_rec_exists.img_url_md = recipe_details['img_url_md']
        if 'img_url_lg' in recipe_details:
            recipe_rec_exists.img_url_lg = recipe_details['img_url_lg']
        db.session.commit()


def get_nutrients_from_recipe_summary(yummly_summary_result):
    """Given a list of recipes (from yummly summary search), calls function to
        get nutrient details (image urls) as a group of yummly GET requests,
        then parses specific fields to update the existing recipe on BE.
    """

    yummly_recipe_ids = []
    for recipe in yummly_summary_result:
        yummly_recipe_ids.append(recipe['id'])

    if (len(yummly_recipe_ids) > 0):
        for i in range(0, len(yummly_recipe_ids)):
            recipe_details = {}
            recipe1_result = get_yummly_recipe_detail(yummly_recipe_ids[i])
            recipe1_dict = recipe1_result.json()
            recipe_id = recipe1_dict['id']
            nutrient_dict = list_nutrients_to_dict(recipe1_dict['nutritionEstimates'])

            if 'SUGAR' in nutrient_dict:
                sugar = nutrient_dict['SUGAR']
                recipe_details['sugar'] = sugar
            if 'CHOCDF' in nutrient_dict:
                carbs = nutrient_dict['CHOCDF']
                recipe_details['carbs'] = carbs
            if 'ENERC_KCAL' in nutrient_dict:
                calories = nutrient_dict['ENERC_KCAL']
                recipe_details['calories'] = calories
            if 'images' in recipe1_dict and 'hostedMediumUrl' in recipe1_dict['images'][0]:
                img_url_md = recipe1_dict['images'][0]['hostedMediumUrl']
                recipe_details['img_url_md'] = img_url_md
            if 'images' in recipe1_dict and 'hostedLargeUrl' in recipe1_dict['images'][0]:
                img_url_lg = recipe1_dict['images'][0]['hostedLargeUrl']
                recipe_details['img_url_lg'] = img_url_lg

            recipe_details['recipe_id'] = recipe_id
            update_recipe_detail(recipe_details)


def get_yummly_recipe_detail(y_recipe_id):
    """Given yummly_recipe_id, makes a GET request to yummly api for recipe
        details (nutrition breakdown, med/lg image urls). Currently doing test
        to understand the timing of these secondary GET requests.
        Returning JSON object as this is not shown directly in browser.
    """

    get_recipe_url = 'http://api.yummly.com/v1/api/recipe/{recipe_id}'.format(recipe_id=y_recipe_id)

    headers = {'X-Yummly-App-ID': consumer_id,
               'X-Yummly-App-Key': consumer_key}
    # sending get request and saving the response as response object
    response = requests.get(url=get_recipe_url, headers=headers, timeout=5)
    return response

    if response.ok:
        return response
    else:
        return {"status_code": response.status_code, "error": "recipe details could not be retrieved"}


def twilio_sms(email):
    """Helper that triggers a twilio api SMS request (with hard-coded from and
        to phone numbers)
    """

    account_sid = os.environ['TWILIO_TEST_SID']
    auth_token = os.environ['TWILIO_AUTH_TOKEN']
    user_rec = db.session.query(User).filter(User.email == email).first()
    # incoming is the recipient phone
    incoming_sms_num = user_rec.phone
    # outgoing is the sender phone (twilio number)
    outgoing_sms_num = os.environ['OUTGOING_SMS_NUM']
    body = 'Hi there, here is a link of recommended recipes for this week. http://localhost:5000'

    client = Client(account_sid, auth_token)

    message = client.messages.create(
        incoming_sms_num,
        body=body,
        from_=outgoing_sms_num)

    # TODO would prefer to store this as log
    # response = {"sid": message.sid, "to": message.to, "status": message.status}
    print(message.sid)
    print(message.to)
    print(message.status)


def list_nutrients_to_dict(nutrient_list):
    """Given list of nutrient objects (from 3rd party api resp), returns a
        dictionary where the key is the nutrient 'attribute' value and key
        value is the nutrient object. Currently returns a mini nutrient dict
        just for the sugar, carb values (g), calories but the nutrients list
        will probably grow. Used to extrapolate json data to put in db.

    >>> list_nutrients_to_dict(test_nutrient_list)
    {'CHOCDF': 25.33, 'ENERC_KCAL': 150, 'SUGAR': 12.13}

    >>> list_nutrients_to_dict(test_nutrient_no_cal_carb)
    {'SUGAR': 12.13}

    >>> list_nutrients_to_dict(test_nutrient_no_sugar)
    {'CHOCDF': 25.33, 'ENERC_KCAL': 150}

    """

    nutrient_dict = {}
    mini_nutrient_dict = {}
    sugar_attr = 'SUGAR'
    carb_attr = 'CHOCDF'
    calorie_attr = 'ENERC_KCAL'  # in yummly recipe detail kcal is used for calories

    for n in nutrient_list:
        nutrient_dict[n['attribute']] = n

    if len(nutrient_dict.keys()) > 0:
        if sugar_attr in nutrient_dict:
            mini_nutrient_dict[sugar_attr] = nutrient_dict[sugar_attr]['value']
        if carb_attr in nutrient_dict:
            mini_nutrient_dict[carb_attr] = nutrient_dict[carb_attr]['value']
        if calorie_attr in nutrient_dict:
            mini_nutrient_dict[calorie_attr] = nutrient_dict[calorie_attr]['value']

    return mini_nutrient_dict


def yummly_get_request(url, params):
    """Given a url (string) and request params (dictionary), returns the json
        response.
    """

    headers = {'X-Yummly-App-ID': consumer_id,
               'X-Yummly-App-Key': consumer_key}
    response = requests.get(url=url, params=params, headers=headers, timeout=5)

    if response.ok:
        # switched to response.content because of test mock expectations
        return response.content
    else:
        return '{"status_code": {status_code}, "error": "recipes could not be retrieved"}'.format(status_code=response.status_code)


# DB helper methods

def get_user_id(email):
    """Given a user's email, returns the user_id. If there was an issue with
        the db query, return None.
    """

    user_id = None
    user_rec = db.session.query(User).filter(User.email == email).first()
    if user_rec is not None:
        user_id = user_rec.user_id
    return user_id


def get_recipe_setting(user_id):
    """Given a user_id, returns the recipe setting table object.
    """

    recipe_setting_rec = db.session.query(Recipe_Settings).filter(Recipe_Settings.user_id == user_id).first()
    return recipe_setting_rec


if __name__ == "__main__":
    # We have to set debug=True here, since it has to be True at the
    # point that we invoke the DebugToolbarExtension
    app.debug = False

    connect_to_db(app)

    # Use the DebugToolbar
    # DebugToolbarExtension(app)

    app.run(host="0.0.0.0")

    # REFACTOR move to another file
    import doctest
    doctest.testmod()
