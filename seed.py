from sqlalchemy import func
from model import (Allergy, Excluded_Ingredient, User, UserRecipe,
                   Cuisine_Type, Recipe_Settings, RecipeSettingsCuisine,
                   UserAllergy, Recipe)
# from model import Excluded_Ingredient
# from model import User
# from model import UserRecipe
# from model import Cuisine_Type
# from model import Recipe_Settings

from model import connect_to_db, db
from server import app


def load_allergies():
    """Loads allergies from u.allergy into db"""

    # delete rows in existing table
    Allergy.query.delete()

    for row in open("seed_data/u.allergy"):
        row = row.strip()
        # unpacking
        allergy_id, short_description = row.split("|")
        allergy = Allergy(allergy_id=allergy_id,
                          short_description=short_description)

        db.session.add(allergy)

    db.session.commit()

def load_ingredients():
    """Loads ingredients from u.ingredients into db"""

    # del rows in exising table
    Excluded_Ingredient.query.delete()

    for row in open("seed_data/u.ingredients"):
        row = row.strip()
        excluded_ingredient_term, search_value = row.split("|")
        excluded_ingredient = Excluded_Ingredient(excluded_ingredient_term=excluded_ingredient_term,
                                                  search_value=search_value)

        db.session.add(excluded_ingredient)

    db.session.commit()


def load_test_user():
    """Loads one user with user_id = 1. Used for mvp testing before implement
        session auth. TODO remove this after session auth.
    """

    UserAllergy.query.delete()
    RecipeSettingsCuisine.query.delete()
    Recipe_Settings.query.delete()
    UserRecipe.query.delete()
    User.query.delete()

    new_user = User(user_id=9999, email="c@c.com", password="pwd",
                    phone="0")
    db.session.add(new_user)
    db.session.commit()


def load_test_recipes():
    """Loads default recipes to show for newly registered users (before their
        personalized search results are ready).
    """

    UserRecipe.query.delete()
    Recipe.query.delete()

    for row in open("seed_data/u.recipe"):
        row = row.strip()
        # yummly_recipe_id, name, carbs, sugar, calories, img_url_sm, img_url_md, img_url_lg, total_time  = 
        recipe_list = row.split("|")

        if len(recipe_list[0]):
            yummly_recipe_id = recipe_list[0].strip()
        if len(recipe_list[1]):
            name = recipe_list[1].strip()
        else:
            name = ''
        print 'name', name
        if len(recipe_list[2]):
            carbs = recipe_list[2].strip()
        else:
            carbs = None
        if len(recipe_list[3]):
            sugar = recipe_list[3].strip()
        else:
            sugar = None
        if len(recipe_list[4]):
            calories = recipe_list[4].strip()
        else:
            calories = None
        if len(recipe_list[5]):
            img_url_sm = recipe_list[5].strip()
        else:
            img_url_sm = ''
        if len(recipe_list[6]):
            img_url_md = recipe_list[6].strip()
        else:
            img_url_md = ''
        if len(recipe_list[7]):
            img_url_lg = recipe_list[7].strip()
        else:
            img_url_lg = ''
        if len(recipe_list[8]):
            total_time = recipe_list[8].strip()
        if len(recipe_list[9]):
            ingredients = recipe_list[9].strip().split(',')

        recipe = Recipe(yummly_recipe_id=yummly_recipe_id, name=name,
                        carbs=carbs, sugar=sugar, calories=calories,
                        img_url_sm=img_url_sm, img_url_md=img_url_md,
                        img_url_lg=img_url_lg, total_time=total_time,
                        ingredients=ingredients)
        db.session.add(recipe)

    db.session.commit()


def load_test_recipe_settings():
    """Loads one recipe settings rec for user with user_id = 1. Used for mvp
        testing before implement session auth. TODO remove this after session
        auth.
    """

    Recipe_Settings.query.delete()

    new_rec_setting = Recipe_Settings(recipe_settings_id=9999999,
                                      max_carbs=25,
                                      max_sugar=35,
                                      notification_day="Saturday",
                                      user_id=9999)
    db.session.add(new_rec_setting)
    db.session.commit()


def load_test_cuisines():
    """Loads a few test cuisines. Used for mvp testing. TODO remove.
    """

    Cuisine_Type.query.delete()

    for row in open("seed_data/u.cuisine"):
        row = row.strip()
        # unpacking
        cuisine_name, search_value = row.split("|")
        cuisine = Cuisine_Type(cuisine_name=cuisine_name,
                               search_value=search_value)

        db.session.add(cuisine)

    db.session.commit()


# def load_test_cuisines():
#     """Loads a few test cuisines. Used for mvp testing. TODO remove.
#     """

#     Cuisine_Type.query.delete()

#     new_cuisine = Cuisine_Type(cuisine_name="asian")
#     new_cuisine2 = Cuisine_Type(cuisine_name="italian")
#     new_cuisine3 = Cuisine_Type(cuisine_name="american")
#     new_cuisine4 = Cuisine_Type(cuisine_name="mexican")
#     db.session.add_all([new_cuisine, new_cuisine2, new_cuisine3, new_cuisine4])
#     db.session.commit()


# only run if script run explicitly
if __name__ == "__main__":
    connect_to_db(app)

    db.create_all()

    # mvp only
    load_test_user()
    load_test_recipes()

    load_allergies()
    load_ingredients()

    # mvp only
    load_test_cuisines()
    load_test_recipe_settings()
